<?php
class Gento_Invoice_Model_Order_Pdf_Invoice extends Mage_Sales_Model_Order_Pdf_Invoice
{



    public function getPdf($invoices = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');

        $pdf = new Zend_Pdf();
        $this->_setPdf($pdf);
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($invoices as $invoice) {
            if ($invoice->getStoreId()) {
                Mage::app()->getLocale()->emulate($invoice->getStoreId());
                Mage::app()->setCurrentStore($invoice->getStoreId());
            }
            $page  = $this->newPage();

            /*$pdf->pages[0] = $page;*/

            $order = $invoice->getOrder();
            /* Add image */
            $this->insertLogo($page, $invoice->getStore());
            /* Add address */
            $this->insertAddress($page, $invoice->getStore());
            /* Add head */
            $this->insertOrder(
                $page,
                $order,
                Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID, $order->getStoreId())
            );
            /* Add document text and number */
            $this->insertDocumentNumber(
                $page,
                Mage::helper('sales')->__('Invoice # ') . $invoice->getIncrementId()
            );
            /* Add table */
            $this->_drawHeader($page);
            /* Add body */
            foreach ($invoice->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                /* Draw item */
                $this->_drawItem($item, $page, $order);
                $page = end($pdf->pages);
            }
            /* Add totals */
            $this->insertTotals($page, $invoice);
            if ($invoice->getStoreId()) {
                Mage::app()->getLocale()->revert();
            }

            // insert mds page
            $page  = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
            $pdf->pages[] = $page;

            $this->y = 750;
            $x = 30;
            $image1 = Mage::getConfig()->getOptions()->getSkinDir().DS.'frontend/gento/default/images/logo.png';
            if (is_file($image1)) {
                $image = Zend_Pdf_Image::imageWithPath($image1);
                $page->drawImage($image, $x+220, $this->y, $x+300, $this->y+40);
            }

            $this->y -=25;
            $this->_setFontRegular($page, 10);
            $page->drawText(Mage::helper('sales')->__('Need to return your purchase?'), $x+210, $this->y, 'UTF-8');
            //$this->y -=18;
//            $page->drawText(Mage::helper('sales')->__('Come into a Singapore mds store or send by post within 14 days.'), $x+140, $this->y, 'UTF-8');
            $this->y -=18;
            $page->drawLine($x, $this->y, $x + 530, $this->y);
            $this->y -=20;
            $this->_setFontBold($page, 10);
            $page->drawText(Mage::helper('sales')->__('LOCAL RETURNS WITHIN SINGAPORE'), $x+30, $this->y, 'UTF-8');
            $this->y -=20;
            $this->_setFontRegular($page, 10);
            $page->drawText(Mage::helper('sales')->__('1.  Stick returns address label & re-seal the parcel.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('2.  Bring your parcel to any post office. We highly encourage you to opt for registered so you can track your parcel.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('3.  You will be notified via email once we received your parcel.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('4.  Please allow up to 14 working days to process your returns.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('5.  We will email you to let you know once we have processed your credit voucher.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('6.  MDS Management reserves the right to reject your returned item if it does not comply with our returns policy. '), $x+50, $this->y, 'UTF-8');
            $this->y -=20;
            $this->_setFontBold($page, 10);
            $page->drawText(Mage::helper('sales')->__('INTERNATIONAL RETURNS'), $x+30, $this->y, 'UTF-8');
            $this->y -=20;
            $this->_setFontRegular($page, 10);
            $page->drawText(Mage::helper('sales')->__('1.  Stick returns address label & re-seal the parcel.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('2.  Bring your parcel to any post office. We highly encourage you to opt for registered so you can track your parcel.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('3.  You will be notified via email once we received your parcel.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('4.  Please allow up to 21 days to process your returns.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('5.  We will email you to let you know once we have processed your credit voucher.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('6.  MDS Management reserves the right to reject your returned item if it does not comply with our returns policy.'), $x+50, $this->y, 'UTF-8');
            $this->y -=20;
            $page->drawLine($x, $this->y, $x + 530, $this->y);
            $this->y -=20;
            $this->_setFontBold($page, 10);
            $page->drawText(Mage::helper('sales')->__('RETURNS POLICY'), $x+30, $this->y, 'UTF-8');
            $this->y -=20;
            $this->_setFontRegular($page, 10);
            $page->drawText(Mage::helper('sales')->__('1.  Your item(s) should be in their original packaging with price tag intact, unwashed and unworn.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('2.  You must include your order form in the parcel that you return.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('3.  Return your parcel to our office address:'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $this->_setFontBold($page, 10);
            $page->drawText(Mage::helper('sales')->__('MDS'), $x+60, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('10 Changi South Street 2, #04-01, Singapore 486596.'), $x+65, $this->y, 'UTF-8');
            $this->y -=12;
            $this->_setFontRegular($page, 10);
            $page->drawText(Mage::helper('sales')->__('4.  No refunds allowed. For all return items, we will issue you a credit voucher to be utilized for your next purchase.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('5.  We do not accept returns for sale items. No refunds allowed for sale merchandise.'), $x+50, $this->y, 'UTF-8');
            $this->y -=12;
            $page->drawText(Mage::helper('sales')->__('6.  In event of missing or incorrect item, kindly contact us at customers@mdscollections.com.'), $x+50, $this->y, 'UTF-8');
            // end insert mds
        }


        $this->_afterGetPdf();
        return $pdf;
    }


}