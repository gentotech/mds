<?php
/**
 * Magemallow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Magemallow
 * @package     Magemallow_BetterProductSorter
 * @copyright   Copyright (c) 2014 KOMMA-D (http://www.komma-d.de)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Magemallow_BetterProductSorter_Block_Sort extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('catalog_category_products');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        $this->setTemplate('betterproductsorter/grid.phtml');
    }

    public function getCategory()
    {
        return Mage::registry('category');
    }

    public function prepareCollection()
    {
        if ($this->getCategory()->getId()) {
            $this->setDefaultFilter(array('in_category' => 1));
        }

        $showStatus = explode(',', $this->getConfigData('show_status'));
        $showVisibility = explode(',', $this->getConfigData('show_visibility'));
        $productIds = $this->_getSelectedProducts();
        if (empty($productIds)) {
            $productIds = 0;
        }

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('status')
            ->addAttributeToSelect('visibility')
            ->addAttributeToSelect('small_image')
            ->addStoreFilter($this->getRequest()->getParam('store'))
            ->addFieldToFilter('status', $showStatus)
            ->addFieldToFilter('visibility', $showVisibility)
            ->addFieldToFilter('entity_id', array('in' => $productIds))
            ->joinField('position',
                'catalog/category_product',
                'position',
                'product_id=entity_id',
                'category_id='.(int) $this->getRequest()->getParam('id', 0),
                'left')
            ->joinTable(
                array('stock' => 'cataloginventory/stock_item'),
                'product_id=entity_id',
                array('qty', 'manage_stock', 'use_config_manage_stock'),
                '{{table}}.stock_id=1',
                'left'
            )
            ->setOrder('position', 'ASC');

        $this->setCollection($collection);

        if ($this->getConfigData('show_instock') == 1) {
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

            foreach ($this->getCollection() as $product) {
                if ($product->getTypeId() == 'configurable') {
                    $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
                    $simpleCollection = $conf->getUsedProductCollection()->addAttributeToSelect('*')
                        ->addFilterByRequiredOptions();
                    $inStock = false;
                    foreach ($simpleCollection as $simpleProduct) {
                        if ($simpleProduct->getStockItem()->getQty() > 0) {
                            $inStock = true;
                        }
                    }
                    if (!$inStock) {
                        $this->getCollection()->removeItemByKey($product->getId());
                    }
                }
            }
        }

        return $this->getCollection();
    }

    protected function _getSelectedProducts()
    {
        $products = $this->getRequest()->getPost('selected_products');
        if (is_null($products)) {
            $products = $this->getCategory()->getProductsPosition();
            return array_keys($products);
        }
        return $products;
    }

    public function getConfigData($key)
    {
        return Mage::helper('betterproductsorter')->getConfigData($key);
    }

    public function getNotAvailableClass($product)
    {
        if (($product->getStatus() != 1) || ($product->getVisibility() == 1)) {
            return ' class="notavailable"';
        } else {
            return '';
        }
    }

    public function getImageUrl($product)
    {
        if ($product->getSmallImage() != 'no_selection' && $product->getSmallImage()) {
            try {
                $imageUrl = Mage::helper('catalog/image')->init($product, 'small_image')
                    ->resize($this->getConfigData('image_size'));
            } catch (Exception $e) {
                $imageUrl = $this->getSkinUrl('images/catalog/product/placeholder/thumbnail.jpg',
                    array('_area' => 'frontend'));
            }                                    
        } else {
            $imageUrl = $this->getSkinUrl('images/catalog/product/placeholder/thumbnail.jpg',
                array('_area' => 'frontend'));
        }
        return $imageUrl;
    }

    public function getTitle($product)
    {
        $title = $product->getName();

        if ($this->getConfigData('show_products_data')) {
            $title .= '<hr>SKU: ' . $product->getSku();

            if ($this->getConfigData('qty_in_tooltip')) {
                if ($product->getUseConfigManageStock() == 0 && $product->getManageStock == 0) {
                    $title .= '<hr>' . $this->__('No stock data');
                } else {
                    if ($product->getTypeId() == 'configurable') {
                        $conf = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
                        $simpleCollection = $conf->getUsedProductCollection()->addAttributeToSelect('*')
                            ->addFilterByRequiredOptions();
                        $qty = 0;
                        foreach ($simpleCollection as $simpleProduct) {
                            $qty += $simpleProduct->getStockItem()->getQty();
                        }
                        if ($qty > 0) {
                            $title .= '<hr>' . $this->__('In Stock (Quantity: %s)', $qty);
                        } else {
                            $title .= '<hr>' . $this->__('Out of Stock');
                        }
                    } else {
                        if ($product->getQty() > 0) {
                            $title .= '<hr>' . $this->__('In Stock (Quantity: %s)',
                                substr($product->getQty(), 0, strrpos($product->getQty(), '.')));
                        } else {
                            $title .= '<hr>' . $this->__('Out of Stock');
                        }
                    }
                }
            }
        }

        return $this->htmlEscape($title);
    }
}