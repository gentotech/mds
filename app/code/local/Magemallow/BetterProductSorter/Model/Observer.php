<?php
/**
 * Magemallow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Magemallow
 * @package     Magemallow_BetterProductSorter
 * @copyright   Copyright (c) 2014 KOMMA-D (http://www.komma-d.de)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Magemallow_BetterProductSorter_Model_Observer
{
    public function addTab($observer)
    {
        $tabs = $observer->getTabs();
        if (Mage::helper('betterproductsorter')->getConfigData('enabled')) {
            $tabs->addTab('betterproductsorter', array(
                'label'   => Mage::helper('betterproductsorter')->__('Better Product Sorter'),
                'content' => $tabs->getLayout()->createBlock('betterproductsorter/sort', 'product.sorter')->toHtml()
            ));
        }
    }
}