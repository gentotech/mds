<?php
/**
 * Magemallow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category    Magemallow
 * @package     Magemallow_BetterProductSorter
 * @copyright   Copyright (c) 2014 KOMMA-D (http://www.komma-d.de)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Magemallow_BetterProductSorter_Model_Source_Imagesize
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => '60',
                'label' => Mage::helper('betterproductsorter')->__('Small (60px)')
            ),
            array(
                'value' => '80',
                'label' => Mage::helper('betterproductsorter')->__('Medium (80px)')
            ),
            array(
                'value' => '120',
                'label' => Mage::helper('betterproductsorter')->__('Big (120px)')
            ),
        );
    }
}
