<?php
class Mage_Adminhtml_Block_Report_Product_Sold_Renderer_Quantity extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        //var_dump($row->getData());die;
        $product_id = $row->getData('entity_id');
        $stock_id = $this->getColumn()->getStockid();
        $_fromdate = date('Y-m-d h:i:s',strtotime($this->getColumn()->getFromdate()));
        $_todate =  date('Y-m-d h:i:s',strtotime($this->getColumn()->getTodate()));
        //var_dump($_fromdate,$_todate);die;
        //Get all the warehouses
        //var_dump($row->getData());
//        $product = Mage::getResourceModel('reports/report_collection')
//            ->addOrderedQty()
//            ->addAttributeToFilter('entity_id', array('eq' => $product_id))
//            ->setPeriodType('year')
//            ->setOrder('ordered_qty', 'desc')->getFirstItem();
////        //echo $product;
//        var_dump($product);
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = "SELECT
          SUM(order_items.qty_ordered) AS `ordered_qty`,
          `order_items`.`name` AS `order_items_name`,
          `order_items`.`product_id` AS `entity_id`,
          `e`.`entity_type_id`,
          `e`.`attribute_set_id`,
          `e`.`type_id`,
          `e`.`sku`,
          `e`.`has_options`,
          `e`.`required_options`,
          `e`.`created_at`,
          `e`.`updated_at`
        FROM
          `sales_flat_order_item` AS `order_items`
          INNER JOIN `sales_flat_order` AS `order`
            ON `order`.entity_id = order_items.order_id
            AND `order`.state <> 'canceled'
          LEFT JOIN `catalog_product_entity` AS `e`
            ON (
              e.type_id NOT IN (
                'grouped'
              )
            )
            AND e.entity_id = order_items.product_id
            AND e.entity_type_id = 4
        WHERE (parent_item_id IS NULL)
          AND (`e`.`entity_id` = '$product_id') AND (order_items.store_id=$stock_id)";
        if(isset($_fromdate))
            $query .=' AND order_items.created_at >"'.$_fromdate.'"';
        if(isset($_todate))
            $query .=' AND order_items.created_at <"'.$_todate.'"';
        $query.="GROUP BY `order_items`.`product_id`
        HAVING (SUM(order_items.qty_ordered) > 0)
        ORDER BY `ordered_qty` DESC ";

        $arrWarehouse = $readConnection->fetchAll($query);
        //var_dump($arrWarehouse);
        return ($arrWarehouse[0]['ordered_qty']!=null)?intval($arrWarehouse[0]['ordered_qty']):'0';
    }
}
?>