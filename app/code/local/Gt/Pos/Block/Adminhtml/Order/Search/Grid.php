<?php

class Gt_Pos_Block_Adminhtml_Order_Search_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('searchGrid');
		$this->setDefaultSort('name');
        $this->setUseAjax(true);
        $this->setRowClickCallback(false);
        $this->setSaveParametersInSession(true);
    }
	
	protected function _getStore() {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }  

	protected function _prepareCollection() {
        $store = $this->_getStore();				
		$collection = Mage::getModel('catalog/product')->getCollection()
				->addAttributeToSelect('sku')
				->addAttributeToSelect('barcode')
				->addAttributeToSelect('name')
				->addAttributeToSelect('attribute_set_id')
                ->addFieldToFilter('type_id','simple');

		 if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $collection->joinField('qty',
                'cataloginventory/stock_item',
                'qty',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left');
        }

        if ($store->getId()) {
            $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
            $collection->addStoreFilter($store);
            // $collection->joinAttribute('custom_name', 'catalog_product/name', 'entity_id', null, 'inner', $store->getId());
            $collection->joinAttribute(
                'name',
                'catalog_product/name',
                'entity_id',
                null,
                'inner',
                $adminStore
            );
            
			$collection->joinAttribute(
                'status',
                'catalog_product/status',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute(
                'visibility',
                'catalog_product/visibility',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute(
                'price',
                'catalog_product/price',
                'entity_id',
                null,
                'left',
                $store->getId()
            );
        } else {
            $collection->addAttributeToSelect('price');
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
            $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        }
        
        $this->setCollection($collection);

        parent::_prepareCollection();
        
        $this->getCollection()->addWebsiteNamesToResult();
        return $this;
    }

    protected function _addColumnFilterToCollection($column) {

        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {

                $this->getCollection()->joinField('websites', 'catalog/product_website', 'website_id', 'product_id=entity_id', null, 'left');
            }
        }

        parent::_addColumnFilterToCollection($column);
        
    }

    protected function _prepareColumns() {
	
		$this->addColumn('barcode', array(
			'header' => Mage::helper('catalog')->__('Barcode'),
			'width' => '70',			
			'sortable'  => false,
			'index' => 'barcode'
        ));
		
		$this->addColumn('name', array(
            'header' => Mage::helper('catalog')->__('Name'),
            'index' => 'name'
        ));  
		
		/* $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
            ->load()
            ->toOptionHash();
		
		$this->addColumn('set_name',
            array(
                'header'=> Mage::helper('catalog')->__('Attrib. Set Name'),
                'width' => '100px',
                'index' => 'attribute_set_id',
                'type'  => 'options',
                'options' => $sets,
        )); */
	
		$this->addColumn('sku', array(
            'header' => Mage::helper('catalog')->__('SKU'),
            'width' => '100',
            'index' => 'sku'
        ));
      
        $store = $this->_getStore();
        $this->addColumn('price',
            array(
                'header'=> Mage::helper('catalog')->__('Price'),
                'type'  => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'price',
        ));
		
		if (Mage::helper('catalog')->isModuleEnabled('Mage_CatalogInventory')) {
            $this->addColumn('qty',
                array(
                    'header'=> Mage::helper('catalog')->__('Qty'),
                    'width' => '100px',
					'type'  => 'number',
                    'filter'    => false,
					'sortable' => false,
                    'index' => 'qty',
            ));
        }
		
		$this->addColumn('visibility',
            array(
                'header'=> Mage::helper('catalog')->__('Visibility'),
                'width' => '70px',
                'index' => 'visibility',
                'type'  => 'options',
                'options' => Mage::getModel('catalog/product_visibility')->getOptionArray(),
        ));
		
		$this->addColumn('status',
            array(
                'header'=> Mage::helper('catalog')->__('Status'),
                'width' => '70px',
                'index' => 'status',
                'type'  => 'options',
                'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));
		
		$this->addColumn('status',
            array(
                'header'=> Mage::helper('catalog')->__('Status'),
                'width' => '70px',
                'index' => 'status',
                'type'  => 'options',
                'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
        ));
		
		$this->addColumn('action', array(  
			'header'=> Mage::helper('catalog')->__('Action'),
            'width' => '40',     
			'align' => 'center',
            'filter'    => false,
			'index' => 'entity_id',
            'renderer'  => 'pos/adminhtml_order_search_renderer_add',
        ));

        return parent::_prepareColumns();
    }	

    public function getGridUrl() {
        return $this->getUrl('*/*/productgrid', array('_current' => true));
    }
}