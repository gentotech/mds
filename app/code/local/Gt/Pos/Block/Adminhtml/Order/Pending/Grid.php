<?php
/** 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @Author     Ocean <ocean@gentotech.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Gt_Pos_Block_Adminhtml_Order_Pending_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('posPendingGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Add save order button to pending grid
     * @return string
     */
    public function getMainButtonsHtml()
    {
        $saveOrderButton = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'     => Mage::helper('adminhtml')->__('Save Order'),
                'onclick'   => 'Pos.Order.saveOrder()',
            ));
        return $saveOrderButton->toHtml().parent::getMainButtonsHtml();
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('sales/quote_collection')
            ->addFieldToFilter('is_active',0)
            ->addFieldToFilter('is_pos',1)
//            ->addFieldToFilter('customer_id',array('neq' => null))
            ->addFieldToFilter('entity_id',array('neq' => Mage::getSingleton('pos/order_session')->getQuote()->getId()))
            ->addFieldToFilter('items_count',array('gt' => 0))
        ;

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('customer_name',array(
            'header'    => Mage::helper('pos')->__('Customer name'),
            'renderer'  => 'Gt_Pos_Block_Adminhtml_Order_Pending_Renderer_Customername',
            'index'     => 'concat(customer_firstname, " ", customer_lastname)',
        ));

        $this->addColumn('qty',array(
            'header'    => Mage::helper('pos')->__('Qty'),
            'index'     => 'items_qty',
            'type'      => 'number'
        ));

        $this->addColumn('subtotal',array(
            'header'    => Mage::helper('pos')->__('Subtotal'),
            'index'     => 'subtotal',
            'type'      => 'currency',
            'currency'  => 'order_currency_code',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('pos')->__('Created At'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '120px',
        ));

        $this->addColumn('action',
            array(
                'header'    => Mage::helper('pos')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'renderer'  => 'Gt_Pos_Block_Adminhtml_Order_Pending_Renderer_Action',
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
            ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_id');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('pos')->__('Delete'),
            'url'      => $this->getUrl('*/*/deleteOrder'),
            'confirm'  => Mage::helper('pos')->__('Are you sure?')
        ));
        return $this;
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/pendinggrid', array('_current' => true));
    }
}