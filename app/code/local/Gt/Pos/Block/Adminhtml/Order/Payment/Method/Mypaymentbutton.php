<?php
class Gt_Pos_Block_Adminhtml_Order_Payment_Method_Mypaymentbutton extends Mage_Payment_Block_Form_Cc
{
	protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('gt_pos/order/payment/method/mypaymentbutton.phtml');
    }
}