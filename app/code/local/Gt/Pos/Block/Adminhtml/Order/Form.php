<?php 
/**
 * Order Form block
 *
 * @category   Gt
 * @package    Gt_Pos
 * @author     Gentotech <http://www.gentotech.com/>
 */

class Gt_Pos_Block_Adminhtml_Order_Form extends Gt_Pos_Block_Adminhtml_Order_Abstract {

    public function __construct() {       

        parent::__construct();
    }
	
	public function getShippingMethod(){
		return $this->getQuote()->getShippingAddress()->getShippingMethod();
	}
	
	public function getPaymentMethod(){
		return $this->getQuote()->getPayment()->getMethod();
	}
	
	/**
     * Check whether it is single store mode
     *
     * @return bool
     */
    public function isSingleStoreMode()
    {
        if (!Mage::app()->isSingleStoreMode()) {
               return false;
        }
        return true;
    }
}