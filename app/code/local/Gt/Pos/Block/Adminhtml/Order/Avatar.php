<?php
/*
 * Date: 14/12/12
 * Time: 10:17
 * @author Ocean <ocean@gentotech.com>
 */
class Gt_Pos_Block_Adminhtml_Order_Avatar extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{
    public function getAvatar(){
        if($this->getUser()){
            return Mage::helper('pos')->getUserAvatar($this->getUser());
        }
    }

    public function getUser()
    {
        if (Mage::getSingleton('admin/session')->getUser()) {
            return Mage::getSingleton('admin/session')->getUser();
        }
        return false;
    }
}