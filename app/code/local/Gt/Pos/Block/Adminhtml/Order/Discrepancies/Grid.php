<?php
class Gt_Pos_Block_Adminhtml_Order_Discrepancies_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('grid');
        $this->setDefaultSort('increment_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        //$store = $this->_getStore();
        $collection = Mage::getModel('pos/discrepancies')->getCollection()
            ->addFieldToSelect('created_at')
            ->addFieldToSelect('start_day')
            ->addFieldToSelect('end_day')
            ->addFieldToSelect('payment_method')
            ->addFieldToSelect('sales_captured')
            ->addFieldToSelect('discrepancies');
            //->addFieldToFilter('type_id','simple');



        $this->setCollection($collection);

        return parent::_prepareCollection();

        //$this->getCollection()->addWebsiteNamesToResult();
        //return $this;
    }

    protected function _prepareColumns() {

        $this->addColumn('created_at', array(
            'header' => Mage::helper('catalog')->__('Created at'),
            'width' => '70',
            'type' => 'datetime',
            //'sortable'  => false,
            'index' => 'created_at'
        ));

        $this->addColumn('start_day', array(
            'header' => Mage::helper('catalog')->__('Start of day'),
            'index' => 'start_day'
        ));

        $this->addColumn('end_day', array(
            'header' => Mage::helper('catalog')->__('End of day'),
            'index' => 'end_day'
        ));

        $this->addColumn('payment_method', array(
            'header' => Mage::helper('catalog')->__('Payment method'),
            'index' => 'payment_method'
        ));

        $this->addColumn('sales_captured', array(
            'header' => Mage::helper('catalog')->__('Sales captured'),
            'index' => 'sales_captured'
        ));

        $this->addColumn('discrepancies', array(
            'header' => Mage::helper('catalog')->__('discrepancies'),
            'index' => 'discrepancies'
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

}