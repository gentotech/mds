<?php
class Gt_Pos_Block_Adminhtml_Order_Search extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {


        $this->_blockGroup = 'pos';
        $this->_controller = 'adminhtml_order_search';

        parent::__construct();
    }

    public function getGridHtml() {
        return $this->getChildHtml('grid');
    }
}