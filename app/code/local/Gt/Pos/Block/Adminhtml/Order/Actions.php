<?php
/*
 * Date: 15/12/12
 * Time: 10:01
 * @author Ocean <ocean@gentotech.com>
 */
class Gt_Pos_Block_Adminhtml_Order_Actions extends Gt_Pos_Block_Adminhtml_Order_Total
{
	public function getPaymentMethodName(){
		try{
			$aType = Mage::getSingleton('payment/config')->getCcTypes();
			$sType = $this->getQuote()->getPayment()->getCcType();
			if (isset($aType[$sType])) {
				$name = $aType[$sType];
			}
//			$name = $this->getQuote()->getPayment()->getCcType();
			
			if($name){
				return $name;
			}
			
			return 'n/a';
		}catch(Exception $e){
			return 'n/a';
		}	
	}
}