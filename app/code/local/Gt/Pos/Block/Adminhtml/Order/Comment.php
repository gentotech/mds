<?php
/*
 * Date: 17/12/12
 * Time: 16:54
 * @author Ocean <ocean@gentotech.com>
 */
class Gt_Pos_Block_Adminhtml_Order_Comment extends Gt_Pos_Block_Adminhtml_Order_Abstract
{
	public function getCommentNote()
    {
        return $this->htmlEscape($this->getQuote()->getCustomerNote());
    }
}