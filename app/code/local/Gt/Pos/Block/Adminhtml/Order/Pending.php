<?php
class Gt_Pos_Block_Adminhtml_Order_Pending extends Gt_Pos_Block_Adminhtml_Order_Abstract
{
	protected $_pendingQuotes;
	
	public function __construct()
    {
		if(is_null($this->_pendingQuotes)){
			$this->_pendingQuotes = $this->_getPendingQuotes();
		}
	}
	
	protected function _getPendingQuotes()
    {
		$quotes = Mage::getResourceModel('sales/quote_collection');
		
		$quotes
			->addFieldToFilter('is_active',0)
			->addFieldToFilter('is_pos',1)
			->addFieldToFilter('customer_id',array('neq' => null))
			->addFieldToFilter('entity_id',array('neq' => $this->getQuote()->getId()))
			->addFieldToFilter('items_count',array('gt' => 0))
			->setPageSize(10)
			->setOrder('updated_at','desc');
		return $quotes;
	}
	
	public function getPendingQuotes()
    {
		return $this->_pendingQuotes;
	}
	
	public function getQuoteCustomer($quote)
    {
		$customerId = $quote->getCustomerId();
		
		if($customerId){
			return Mage::getModel('customer/customer')->load($customerId);
		}
	}
	
	public function getCustomerName($quote)
    {
		if($this->getQuoteCustomer($quote)->getId()){
			return $this->getQuoteCustomer($quote)->getName();
		}
		
		return 'n/a';
	}
	
	public function getCustomerGroup($quote)
    {
		if($quote->getCustomerGroupId()){
			$group = Mage::getModel('customer/group')->load($quote->getCustomerGroupId());
			return ($group->getId ? $group->getCode() : 'n/a');
		}
	}
}