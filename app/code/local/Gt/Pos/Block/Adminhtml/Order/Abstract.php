<?php
/*
 * Date: 14/12/12
 * Time: 14:02
 * @author Ocean <ocean@gentotech.com>
 */
class Gt_Pos_Block_Adminhtml_Order_Abstract extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{
    protected function _getSession()
    {
        return Mage::getSingleton('pos/order_session');
    }

    public function getPosCustomerId()
    {
        return $this->_getSession()->getPosCustomerId();
    }
	
	public function getCurrencySymbol()
    {
        return Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
    }

    public function formatPrice($value,$includeContainer = true)
    {
        return $this->getStore()->formatPrice($value,$includeContainer);
    }

    public function getLastScannedProductId()
    {
		if(empty($this->_lastScannedProductId)){
			$this->_lastScannedProductId = $this->_getSession()->getLastScannedProductId();
		}
		return $this->_lastScannedProductId;
    }
	
	public function getLastScannedQuoteItemId()
    {
		if(empty($this->_lastScannedQuoteItemId)){
			$this->_lastScannedQuoteItemId = $this->_getSession()->getLastScannedQuoteItemId();
		}
		return $this->_lastScannedQuoteItemId;
	}

    /**
     * May be deprecated
     * @return bool|Mage_Catalog_Model_Product
     */
    public function getLastScannedProduct()
    {
		$lastItem = $this->getLastScannedItem();
		
		if($lastItem && $lastItem->getProduct()){
			return $lastItem->getProduct();
		}
		
		return false;
	}

    /**
     * Get last item added to cart
     *
     * @return bool|Mage_Sales_Model_Quote_Item
     */
    public function getLastScannedItem()
    {
		$quote = $this->getQuote();
		
		if($quoteId = $this->getLastScannedQuoteItemId()){
			return $quote->getItemById($quoteId);
		}else{
			$product = Mage::getModel('catalog/product')->load($this->getLastScannedProductId());
			return $quote->getItemByProduct($product);
		}
		return false;
	}

    /**
     * Get current admin session user
     * @return mix
     */
    public function getUser()
    {
        if (Mage::getSingleton('admin/session')->getUser()) {
            return Mage::getSingleton('admin/session')->getUser();
        }
        return false;
    }

    /**
     * Check whether current quote is restore
     */
    public function isRestoredOrder()
    {
        return $this->getQuote()->getIsPosSaved();
    }
}