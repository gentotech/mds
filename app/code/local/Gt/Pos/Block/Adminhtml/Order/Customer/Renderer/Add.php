<?php
class Gt_Pos_Block_Adminhtml_Order_Customer_Renderer_Add extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {        
        $customerId = $this->_getValue($row);
		$html = '<a href="javascript:void(0);" title="Select customer" ';
        $html .= Mage::getSingleton('pos/order_session')->getIsPos() ? 'onclick="alert(\'Can\'t choose product in restored order!\')"' : 'onclick="Pos.Order.selectCustomer(\''.$customerId.'\')">';
        $html .= '<img src="'.$this->getSkinUrl('gt_pos/images/add.png').'"/>'
		        .'</a>';

        return $html;
    }
}