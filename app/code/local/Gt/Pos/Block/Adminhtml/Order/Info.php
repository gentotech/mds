<?php
class Gt_Pos_Block_Adminhtml_Order_Info extends Mage_Adminhtml_Block_Sales_Order_Create_Abstract
{
	public function getOrder()
	{
		$order = $this->_getSession()->getOrder();
		
		if($order->getId()){
			//clear order in session
			$this->_getSession()->setOrderId(false);
			
			return $order;
		}
		
		return false;		
	}
}