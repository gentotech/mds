<?php
/*
 * Date: 17/12/12
 * Time: 16:53
 * @author Ocean <ocean@gentotech.com>
 */
class Gt_Pos_Block_Adminhtml_Order_Customer_Info extends Gt_Pos_Block_Adminhtml_Order_Abstract
{
	public function isModeActive($modeCode){
		if($modeCode == 'guest'){
			if($this->_getSession()->getPosCustomerId() == 'guest' || is_null($this->_getSession()->getPosCustomerId())){
				return true;
			}
		}elseif($modeCode == 'new'){
			if($this->_getSession()->getPosCustomerId() == 'new'){
				return true;
			}
		}elseif($modeCode == 'exist'){
			if($this->_getSession()->getCustomerId()){
				return true;
			}
		}
		return false;
	}
}