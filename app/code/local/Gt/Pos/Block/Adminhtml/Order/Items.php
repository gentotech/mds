<?php
/*
 * Date: 14/12/12
 * Time: 17:04
 * @author Ocean <ocean@gentotech.com>
 */
class Gt_Pos_Block_Adminhtml_Order_Items extends Gt_Pos_Block_Adminhtml_Order_Abstract
{
    public function getItems(){
        $items = $this->getQuote()->getAllVisibleItems();
        $oldSuperMode = $this->getQuote()->getIsSuperMode();
        $this->getQuote()->setIsSuperMode(false);
        foreach ($items as $item) {
            // To dispatch inventory event sales_quote_item_qty_set_after, set item qty
            $item->setQty($item->getQty());
            $stockItem = $item->getProduct()->getStockItem();
            if ($stockItem instanceof Mage_CatalogInventory_Model_Stock_Item) {
                if ($item->getProduct()->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {
                    $item->setMessage(Mage::helper('adminhtml')->__('This product is currently disabled.'));
                    $item->setHasError(true);
                }
            }
        }
        $this->getQuote()->setIsSuperMode($oldSuperMode);
        return $items;
    }

    public function hasItems(){
        return $this->getQuote()->hasItems();
    }

    public function isItemHighlight($item){
		$lastItem = $this->getLastScannedItem();
		if($lastItem && $lastItem->getId()){
			return ($lastItem->getId() == $item->getId());
		}        
    }
	
	public function getAppliedRules($item){
		$rule_ids = $item->getData('applied_rule_ids');
		if($rule_ids){
			$rule_ids = explode(',',$rule_ids);
			if(!is_array($rule_ids) && count($rule_ids) > 0){
				return 'No rule';
			}
			$ruleHtml = '<ul class="pos-quote_promotion">';
			foreach($rule_ids as $rule){
				$saleRuleModel = Mage::getModel('salesrule/rule')->load($rule);
				if($saleRuleModel->getId()){
					$ruleHtml .= '<li>'.$saleRuleModel->getName().'</li>';
				}
			}
			$ruleHtml .= '</ul>';
			return $ruleHtml;
		}
		return 'No rule';
	}
}