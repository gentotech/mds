<?php
class Gt_Pos_Block_Adminhtml_Order_discrepancies extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {

        $this->_blockGroup = 'gt_pos';
        //$this->_controller = 'adminhtml_discrepancies';
        $this->_controller = 'adminhtml_pos_order';
        parent::__construct();
    }

    public function getGridHtml() {
        return $this->getChildHtml('grid');
    }
}