<?php
class Gt_Pos_Block_Adminhtml_Order_Pos_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('posGrid');
        $this->setDefaultSort('name');
        $this->setUseAjax(true);
        $this->setRowClickCallback(false);
        $this->setSaveParametersInSession(true);
    }

    protected function _getStore() {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    protected function _prepareColumns() {

        $this->addColumn('created_at', array(
            'header' => Mage::helper('catalog')->__('Created Ate'),
            'width' => '70',
            'sortable'  => false,
            'index' => 'created_at'
        ));

        $this->addColumn('start_day', array(
            'header' => Mage::helper('catalog')->__('Start Of Day'),
            'index' => 'start_day'
        ));

        /* $sets = Mage::getResourceModel('eav/entity_attribute_set_collection')
            ->setEntityTypeFilter(Mage::getModel('catalog/product')->getResource()->getTypeId())
            ->load()
            ->toOptionHash();

        $this->addColumn('set_name',
            array(
                'header'=> Mage::helper('catalog')->__('Attrib. Set Name'),
                'width' => '100px',
                'index' => 'attribute_set_id',
                'type'  => 'options',
                'options' => $sets,
        )); */

        $this->addColumn('end_day', array(
            'header' => Mage::helper('catalog')->__('End Of Day'),
            'width' => '100',
            'index' => 'end_day'
        ));

        $store = $this->_getStore();
        $this->addColumn('payment_method',
            array(
                'header'=> Mage::helper('catalog')->__('Payment Method'),
                'type'  => 'price',
                //'currency_code' => $store->getBaseCurrency()->getCode(),
                'index' => 'payment_method',
            ));


            $this->addColumn('discrepancies',
                array(
                    'header'=> Mage::helper('catalog')->__('Discrepancies'),
                    'width' => '100px',
                    'type'  => 'number',
                    'filter'    => false,
                    'sortable' => false,
                    'index' => 'discrepancies',
                ));



        return parent::_prepareColumns();
    }

}