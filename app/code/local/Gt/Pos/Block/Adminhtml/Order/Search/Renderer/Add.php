<?php 
class Gt_Pos_Block_Adminhtml_Order_Search_Renderer_Add extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{   /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $productId = $this->_getValue($row);

        $html = '<a id="add_product_'.$productId.'" href="javascript:void(0);" onclick="Pos.Order.addProduct('.$productId.')">
                    <img src="'.$this->getSkinUrl('gt_pos/images/add.png').'" alt="Add product">
                </a>';
		
		return $html;
    }
}