<?php
class Gt_Pos_Block_Adminhtml_Order_Payment_Method extends Mage_Adminhtml_Block_Sales_Order_Create_Billing_Method_Form
{
     /**    
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return Mage::getSingleton('pos/order_session')->getQuote();
    }
}