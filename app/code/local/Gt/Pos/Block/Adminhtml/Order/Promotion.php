<?php 

class Gt_Pos_Block_Adminhtml_Order_Promotion extends Gt_Pos_Block_Adminhtml_Order_Abstract {
	protected $_catalogRules = null;
	protected $_cartRules = null;
	
	protected $_salesRuleModel = 'salesrule/rule';
	protected $_catalogRuleModel = 'catalogrule/rule';
	
	public function getAllPromotions()
	{
		$result = array();
		if($catalogRules = $this->getCatalogRules()){
			// $result['catalog_rules'] = $catalogRules;
		}
		if($cartRules = $this->getCartRules()){
			$result['cart_rules'] = $cartRules;
		}
		
		if(!empty($result)){
			return $result;
		}
				
		return false;
	}
	
	public function getCatalogRules(){
		if(empty($this->_catalogRules)){
			$this->_catalogRules = $this->_getRuleCollection($this->_catalogRuleModel);
		}
		
		if($this->_catalogRules){
			return ($this->_catalogRules->count() > 0) ? $this->_catalogRules : false;
		}
		
		return false;
	}
	
	public function getCartRules(){
		if(empty($this->_cartRules)){
			$this->_cartRules = $this->_getRuleCollection($this->_salesRuleModel);
		}
		
		if($this->_cartRules){
			return ($this->_cartRules->count() > 0) ? $this->_cartRules : false;
		}
		
		return false;
	}
	
	public function isHighlight($rule){
		if($rule->getId() && $this->getLastAppliedRules()){
			return in_array($rule->getId(),$this->getLastAppliedRules());
		}	
	}
	
	/**	 
	 *@return array
	 */
	public function getLastAppliedRules(){
		if(empty($this->_lastAppliedRules)){
			$lastItem = $this->getLastScannedItem();		
			if($lastItem){
				$applied_rule_ids = $lastItem->getData('applied_rule_ids');
				if($applied_rule_ids){
					$this->_lastAppliedRules =  explode(',',$applied_rule_ids);
				}
			}
		}
		return $this->_lastAppliedRules;
	}
	
	protected function _getRuleCollection($ruleModel){
		//get now time for compare
		$todayDate = $this->getTime();		
		
		$rules = Mage::getModel($ruleModel)
			->getResourceCollection();
			
		$rules->addWebsitesToResult();
		
		$rules->addFieldToFilter('is_active', array('eq' => 1))
			->addFieldToFilter('from_date', array(
				array('date'=>true, 'to'=> $todayDate),
				array('null' => true)
			))
			->addFieldToFilter('to_date', array(
				array('date'=>true, 'from'=> $todayDate),
				array('null' => true)
			))
			->setOrder('name','ASC');
		return $rules;
	}
	
	public function getTime(){
		return date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time()));
	}
	
	public function getStoreId(){
		return Mage::app()->getStore()->getId();
	}
}