<?php
/*
 * Date: 15/12/12
 * Time: 10:17
 * @author Ocean <ocean@gentotech.com>
 */
class Gt_Pos_Block_Adminhtml_Order_Product_Info extends Gt_Pos_Block_Adminhtml_Order_Abstract
{
    public function getProduct(){
        return $this->getLastScannedProduct();
    }

    public function canShow(){
        if($this->getProduct()){
            return true;
        }else{
            return false;
        }
    }
}