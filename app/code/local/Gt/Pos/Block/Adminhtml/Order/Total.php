<?php
/*
 * Date: 14/12/12
 * Time: 14:09
 * @author Ocean <ocean@gentotech.com>
 */
class Gt_Pos_Block_Adminhtml_Order_Total extends Gt_Pos_Block_Adminhtml_Order_Abstract
{
    public function getCustomerName(){
		if($this->getCustomer()->getId()){
			return $this->getCustomer()->getName();
		}
        return $this->getPosCustomerId() == 'new' ? $this->__('New Customer') : $this->__('Guest');
    }

    public function getCustomerGroup(){
		if($this->getCustomer()->getId()){
			if($this->getCustomer()->getGroupId()){
				$group = Mage::getModel('customer/group')->load($this->getCustomer()->getGroupId());
				if($group->getId()){
					return $group->getCustomerGroupCode();
				}
			}
		}
		return $this->__('N/A');        
    }

    public function getTotal(){
        return $this->getQuote()->getGrandTotal();
    }
}