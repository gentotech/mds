<?php
/*
 * Date: 13/12/12
 * Time: 14:21
 * @author Ocean <ocean@gentotech.com>
 */
class Gt_Pos_Block_Adminhtml_Permissions_User_Edit_Form extends Mage_Adminhtml_Block_Permissions_User_Edit_Form
{
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $this->getForm()->setEnctype('multipart/form-data');
    }
}