<?php
/*
 * Date: 13/12/12
 * Time: 10:50
 * @author Ocean <ocean@gentotech.com>
 */
require_once('Mage/Adminhtml/controllers/Permissions/UserController.php');
class Gt_Pos_Adminhtml_Permissions_UserController extends Mage_Adminhtml_Permissions_UserController
{
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $id = $this->getRequest()->getParam('user_id');
            $model = Mage::getModel('admin/user')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This user no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            //Gentotech: add code for saving avatar
            if (isset($_FILES['avatar']['name']) && ($_FILES['avatar']['name'] != '')
                && ($_FILES['avatar']['size'] != 0) ) {
                try {
                    $uploader = new Varien_File_Uploader('avatar');

                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);

                    $path = Mage::getBaseDir('media') . DS . 'pos' . DS . 'avatar' . DS;

                    //saved the name in DB
                    $fileName = $_FILES['avatar']['name'];

                    $result = $uploader->save($path, $fileName);

                    $data['avatar'] = $result['file'];
                } catch (Exception $e) {
                    $this->_redirect('*/*/edit', array('_current' => true));
                    return $this;
                }
            } elseif (isset($data['avatar']['delete'])) {
                $path = Mage::getBaseDir('media') . DS;
                unlink($path . $data['avatar']['value']);

                $data['avatar'] = '';
            } else {
                if (isset($data['avatar']['value'])) {
                    $data['avatar'] = $data['avatar']['value'];
                }
            }

            $model->setData($data);

            /*
             * Unsetting new password and password confirmation if they are blank
             */
            if ($model->hasNewPassword() && $model->getNewPassword() === '') {
                $model->unsNewPassword();
            }
            if ($model->hasPasswordConfirmation() && $model->getPasswordConfirmation() === '') {
                $model->unsPasswordConfirmation();
            }

            $result = $model->validate();
            if (is_array($result)) {
                Mage::getSingleton('adminhtml/session')->setUserData($data);
                foreach ($result as $message) {
                    Mage::getSingleton('adminhtml/session')->addError($message);
                }
                $this->_redirect('*/*/edit', array('_current' => true));
                return $this;
            }

            try {
                $model->save();
                if ( $uRoles = $this->getRequest()->getParam('roles', false) ) {
                    /*parse_str($uRoles, $uRoles);
                    $uRoles = array_keys($uRoles);*/
                    if ( 1 == sizeof($uRoles) ) {
                        $model->setRoleIds($uRoles)
                            ->setRoleUserId($model->getUserId())
                            ->saveRelations();
                    } else if ( sizeof($uRoles) > 1 ) {
                        //@FIXME: stupid fix of previous multi-roles logic.
                        //@TODO:  make proper DB upgrade in the future revisions.
                        $rs = array();
                        $rs[0] = $uRoles[0];
                        $model->setRoleIds( $rs )->setRoleUserId( $model->getUserId() )->saveRelations();
                    }
                }
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The user has been saved.'));
                Mage::getSingleton('adminhtml/session')->setUserData(false);
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setUserData($data);
                $this->_redirect('*/*/edit', array('user_id' => $model->getUserId()));
                return;
            }
        }
        $this->_redirect('*/*/');
    }
}