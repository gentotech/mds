<?php 
/**
 * Order Form controller
 *
 * @category   Gt
 * @package    Gt_Pos
 * @author     Gentotech <http://www.gentotech.com/>
 */
 
class Gt_Pos_Adminhtml_Pos_OrderController extends Mage_Adminhtml_Controller_Action 
{
	protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('pos');
        return $this;
    }

    /**
     * Orders grid
     */
    public function indexAction()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Orders'));

        $this->_initAction()
            ->renderLayout();
    }

	public function createAction()
    {
        $this->_title($this->__('Point Of Sale'));
        $this->_initAction();
        $this->_initSession()
			->_initDefaultPosData();
        $this->renderLayout();
    }
	
	public function customerGridAction()
	{
		$this->getResponse()->setBody(
            $this->getLayout()->createBlock('pos/adminhtml_order_customer_grid')->toHtml()
        );
	}
	
	public function productGridAction()
	{
		$this->getResponse()->setBody(
            $this->getLayout()->createBlock('pos/adminhtml_order_search_grid')->toHtml()
        );
	}

    public function gridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('pos/adminhtml_order_discrepancies_grid')->toHtml()
        );
    }

    public function pendingGridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('pos/adminhtml_order_pending_grid')->toHtml()
        );
    }

    /**
     * Retrieve session object
     *
     * @return Gt_Pos_Model_Order_Session
     */
    protected function _getSession()
    {
        return Mage::getSingleton('pos/order_session');
    }

    /**
     * Retrieve quote object
     *
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote()
    {
        return $this->_getSession()->getQuote();
    }

    /**
     * Retrieve order create model
     *
     * @return Gt_Pos_Model_Order_Create
     */
    protected function _getOrderCreateModel()
    {
        return Mage::getSingleton('pos/order_create');
    }
	
	protected function _initDefaultPosData()
    {
		if(!$this->_getSession()->getPosCustomerId()){
			$this->_getSession()->setCustomerId('guest');
		}

		return $this;
	}
	
	public function setCustomerMode($customerMode)
    {
		$this->_getSession()->setCustomerMode($customerMode);
		return $this;
	}

	/**
     * Initialize order creation session data
     *
     */
    protected function _initSession()
    {
        if($saveOrder = $this->getRequest()->getPost('save_order')){
            $this->_getSession()
                ->getQuote()
                ->setIsPosSaved(true)
                ->save();

            $this->_getSession()
                ->clear();
//            $this->_getSession()->renewSession();

            //set store_id & customer_id for creating new quote
            $this->_getSession()->setQuote(null);
            $this->_getSession()
                ->setStoreId()
                ->setCustomerId('guest');

            /**
             * Init new quote
             */
            $this->_getSession()->getQuote();
            $this->_getOrderCreateModel()->setQuote($this->_getSession()->getQuote());

            return $this;
        }

        //TODO check the store id, data need restore into session
        if ($quoteId = $this->getRequest()->getPost('restore_order')) {
            $this->_getSession()
                ->setStoreId(1)
                ->setQuoteId((int) $quoteId);
            $this->_getSession()->setCustomerId($this->_getSession()->getQuote()->getCustomerId());
            $this->_reloadQuote();
            return $this;
        }

        /**
         * Identify customer
         */
        if ($customerId = $this->getRequest()->getParam('customer_id')) {
			$currentPosCustomerId = $this->_getSession()->getPosCustomerId();
			if($customerId != $currentPosCustomerId){
				$this->_getSession()->setCustomerId($customerId);
			}
        }

        /**
         * Identify store
         */
        if ($storeId = $this->getRequest()->getParam('store_id')) {
            $this->_getSession()->setStoreId((int) $storeId);
        }

        /**
         * Identify currency
         */
        if ($currencyId = $this->getRequest()->getParam('currency_id')) {
            $this->_getSession()->setCurrencyId((string) $currencyId);
            $this->_getOrderCreateModel()->setRecollect(true);
        }
        return $this;
    }

    /**
     * Processing request data
     *
     */
    protected function _processData()
    {
        return $this->_processActionData();
    }

    protected function _processActionData($action = null)
    {
		$posCustomerId = $this->_getSession()->getPosCustomerId();

        /**
         * Saving order data
         */
        if ($data = $this->getRequest()->getPost('order')) {
            $this->_getOrderCreateModel()->importPostData($data);
        }

        /**
         * Initialize catalog rule data
         */
        $this->_getOrderCreateModel()->initRuleData();

        /**
         * init first billing address, need for virtual products
         */
        $this->_getOrderCreateModel()->getBillingAddress();
		
		if($this->getRequest()->getParam('update_address') == 1)
		{
			if($posCustomerId == 'new'){
                $this->_getQuote()->setCustomer(Mage::getModel('customer/customer'));
                $this->_getQuote()->setCustomerIsGuest(0);
				$emptyAddress = Mage::helper('pos')->getEmptyAddress();
				$this->_getOrderCreateModel()->setBillingAddress($emptyAddress->getData());
				// $this->_getOrderCreateModel()->resetShippingMethod();
			}
			elseif($posCustomerId == 'guest'){
                $this->_getQuote()->setCustomer(Mage::getModel('customer/customer'));
                $this->_getQuote()->setCustomerIsGuest(1);
				$defaultAddress = Mage::helper('pos')->getDefaultAddress();
				$this->_getOrderCreateModel()->setBillingAddress($defaultAddress->getData());
			}
			else{
                //Need assign customer with address change
                $customer = $this->_getSession()->getCustomer();
				$customerAddress = Mage::helper('pos')->getCustomerAddress($this->_getSession()->getCustomer());
                $billingAddress = Mage::getModel('sales/quote_address')
                    ->importCustomerAddress($customerAddress);
				$this->_getQuote()->assignCustomerWithAddressChange($customer,$billingAddress,$billingAddress);
                $this->_getQuote()->setCustomerIsGuest(0);
			}
		}

        /**
         * Flag for using billing address for shipping
		 * deprecated
         */
        if (!$this->_getOrderCreateModel()->getQuote()->isVirtual()) {
            $syncFlag = $this->getRequest()->getPost('shipping_as_billing');
            $shippingMethod = $this->_getOrderCreateModel()->getShippingAddress()->getShippingMethod();
            if (is_null($syncFlag)
                && $this->_getOrderCreateModel()->getShippingAddress()->getSameAsBilling()
                && empty($shippingMethod)
            ) {
                $this->_getOrderCreateModel()->setShippingAsBilling(1);
            } else {
                $this->_getOrderCreateModel()->setShippingAsBilling((int)$syncFlag);
            }
        }

        /**
         * Change shipping address flag
         */
        if (!$this->_getOrderCreateModel()->getQuote()->isVirtual() && $this->getRequest()->getPost('reset_shipping')) {
            $this->_getOrderCreateModel()->resetShippingMethod(true);
        }

		/**
         * Collecting shipping rates
         */
        if (!$this->_getOrderCreateModel()->getQuote()->isVirtual() &&
            $this->getRequest()->getPost('collect_shipping_rates')
        ) {
            $this->_getOrderCreateModel()->collectShippingRates();
        }

		/**
         * check barcode
         */
        if ($barcode = $this->getRequest()->getPost('barcode')) {
			$product = Mage::getModel('catalog/product')->loadByAttribute('barcode',$barcode);
			
			if($product && $product->getId()){
				$this->addProduct($product->getId());
			}else{
				$this->_getSession()->addError('Can\'t find the product!');
			}
        }
		
        /**
         * Adding product to quote
         */
        if ($productId = (int) $this->getRequest()->getPost('add_product')) {
            $this->addProduct($productId);           
        }

        /**
         * Update quote items
         */
        if ($this->getRequest()->getPost('update_items')) {
            $items = $this->getRequest()->getPost('item', array());
            $this->_getOrderCreateModel()->updateQuoteItems($items);
			
			$itemIds = array_keys($items);
			
			if(count($itemIds) > 0){
				$this->setLastScannedQuoteId($itemIds[0]);
				// $this->_getOrderCreateModel()->resetShippingMethod(true);
				$this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
			}
        }

        /**
         * Remove quote item
         */
        $removeItemId = (int) $this->getRequest()->getPost('remove_item');
        if ($removeItemId) {
            $this->removeQuoteItem($removeItemId);
			$this->_getOrderCreateModel()->resetShippingMethod(true);
			// $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
        }

        if ($paymentData = $this->getRequest()->getPost('payment')) {
            $this->_getOrderCreateModel()->getQuote()->getPayment()->addData($paymentData);
        }

        if($isCreateInvoice = $this->getRequest()->getPost('is_create_invoice')){
            $this->_getSession()->setIsCreateInvoice($isCreateInvoice);
        }
//	    $session = $this->_getSession();
//        $quote = $this->_getQuote();
        $this->_getOrderCreateModel()
            ->saveQuote();

        return $this;
    }

    /**
     * Action for retrieving current order data
     */
    public function orderDataAction()
    {
        //TODO save pos customer id to quote so that we can get when restore order
        $orderData = array(
            'customer_id' => $this->_getSession()->getPosCustomerId(),
        );

        $shippingAddress = $this->_getQuote()->getShippingAddress();
        if($shippingAddress->getId()){
            $orderData['shipping_method'] = $shippingAddress->getShippingMethod();
        }

        $payment = $this->_getQuote()->getPayment();
        if($payment->getMethod()){
            $orderData['payment_method'] = $payment->getMethod();
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($orderData));
    }

    public function addProduct($pid,$qty = null){
        try{
            $item = $this->_getOrderCreateModel()->addProduct($pid);
            if (is_string($item)) {
                Mage::throwException($item);
            }
//			$this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->setLastScannedProductId($pid);
        }catch (Exception $e){
            $this->_getSession()->addError($e);
        }
    }

    public function removeQuoteItem($quoteId){
        $this->_getOrderCreateModel()->removeQuoteItem($quoteId);
        $this->setLastScannedProductId(null);
    }
	
	/**
     * Saving quote and create order
     */
    public function placeOrder()
    {
        try {           
            if ($paymentData = $this->getRequest()->getPost('payment')) {
                $this->_getOrderCreateModel()->setPaymentData($paymentData);
                $this->_getOrderCreateModel()->getQuote()->getPayment()->addData($paymentData);
            }

			$invoiceData = $this->getRequest()->getPost('invoice');

            $order = $this->_getOrderCreateModel()
                ->setIsValidate(true)
                ->importPostData($this->getRequest()->getPost('order'))
				->setInvoiceData($invoiceData)
                ->createOrder();
				
			$this->_getQuote()->delete();

            $this->_getSession()->clear();
			
			// $this->_getSession()->addSuccess($this->__('The order has been created.'));
			
			//save order id in session for viewing
			$this->_getSession()->setOrderId($order->getId());
            // $this->loadLayout();
            // $block = $this->getLayout()->getBlock('pos.create.success');

            // if($block){
                // $block->setData('order',$order);
            // }

            // $this->renderLayout();

        } catch (Mage_Payment_Model_Info_Exception $e) {
            $this->_getOrderCreateModel()->saveQuote();
            $message = $e->getMessage();
            if( !empty($message) ) {
                $this->_getSession()->addError($message);
				// $this->getResponse()->setBody(json_encode(array('error' => $message)));				
            }
        } catch (Mage_Core_Exception $e){
			$message = $e->getMessage();
			if( !empty($message) ) {
				$this->_getSession()->addError($message);	
				// $this->getResponse()->setBody(json_encode(array('error' => $message)));	
			}
        } catch (Exception $e){
            $this->_getSession()->addException($e, $this->__('Order saving error: %s', $e->getMessage()));
        }
    }

	/**
     * Saving quote and create order
     */
    public function saveAction()
    {
        try {
            $this->_processActionData('save');
            if ($paymentData = $this->getRequest()->getPost('payment')) {
                $this->_getOrderCreateModel()->setPaymentData($paymentData);
                $this->_getOrderCreateModel()->getQuote()->getPayment()->addData($paymentData);
            }

            $order = $this->_getOrderCreateModel()
                ->setIsValidate(true)
                ->importPostData($this->getRequest()->getPost('order'))
                ->createOrder();

            $this->_getSession()->clear();

            $this->loadLayout();
            $block = $this->getLayout()->getBlock('pos.create.success');

            if($block){
                $block->setData('order',$order);
            }

            $this->renderLayout();

        } catch (Mage_Payment_Model_Info_Exception $e) {
            $this->_getOrderCreateModel()->saveQuote();
            $message = $e->getMessage();
            if( !empty($message) ) {
                // $this->_getSession()->addError($message);
				$this->getResponse()->setBody(json_encode(array('error' => $message)));				
            }
        } catch (Mage_Core_Exception $e){
			$message = $e->getMessage();
			if( !empty($message) ) {
				// $this->_getSession()->addError($message);	
				$this->getResponse()->setBody(json_encode(array('error' => $message)));	
			}
        } catch (Exception $e){
            $this->_getSession()->addException($e, $this->__('Order saving error: %s', $e->getMessage()));
            $this->_redirect('*/*/');
        }
    }

    public function setLastScannedProductId($pid){
        $this->_getSession()->setLastScannedProductId($pid);
		$this->_getSession()->setLastScannedQuoteId(null);
        return $this;
    }
	
	public function setLastScannedQuoteId($id){
        $this->_getSession()->setLastScannedQuoteId($id);
		$this->_getSession()->setLastScannedProductId(null);
        return $this;
    }

    protected function _reloadQuote()
    {
        $id = $this->_getQuote()->getId();
        $this->_getQuote()->load($id);
        return $this;
    }

    /**
     * Loading page block
     */
    public function loadBlockAction()
    {
        $request = $this->getRequest();
        try {
            $this->_initSession()
                ->_processData();

			if($this->getRequest()->getPost('place_order') == 1){
				$this->placeOrder();
			}
        }
        catch (Mage_Core_Exception $e){
            $this->_reloadQuote();
            $this->_getSession()->addError($e->getMessage());
        }
        catch (Exception $e){
            $this->_reloadQuote();
            $this->_getSession()->addException($e, $e->getMessage());
        }

        $asJson= $request->getParam('json');
        $block = $request->getParam('block');

        $update = $this->getLayout()->getUpdate();
        if ($asJson) {
            $update->addHandle('adminhtml_sales_order_create_load_block_json');
        } else {
            $update->addHandle('adminhtml_sales_order_create_load_block_plain');
        }

        if ($block) {
            $blocks = explode(',', $block);
            if ($asJson && !in_array('message', $blocks)) {
                $blocks[] = 'message';
            }

            foreach ($blocks as $block) {
                $update->addHandle('adminhtml_pos_order_create_load_block_' . $block);
            }
        }

        $this->loadLayoutUpdates()->generateLayoutXml()->generateLayoutBlocks();
        $result = $this->getLayout()->getBlock('content')->toHtml();

        $this->getResponse()->setBody($result);
    }

    /**
     * Init order for rendering order information
     * @return bool|Mage_Sales_Model_Order
     */
    protected function _initOrder()
    {
        $id = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($id);

        if (!$order->getId()) {           
            return false;
        }
        Mage::register('sales_order', $order);
        Mage::register('current_order', $order);
        return $order;
    }

    /**
     * Get receipt html
     */
    public function printAction()
	{
		if ($order = $this->_initOrder()) {          
			$this->loadLayout(false);
			$this->renderLayout();
        }else{
			$this->getResponse()->setBody('
				<script>
					alert("Can not print receipt.");
				</script>
			');
		}	
	}

    public function createPosAction()
    {

        $payments = Mage::getModel('payment/config')->getActiveMethods();
        $methods = array();

        $data = $this->getRequest()->getPost();
        foreach ($payments as $paymentCode => $paymentModel) {

            if ($paymentCode == 'ccsave'){
                $types = Mage::getSingleton('payment/config')->getCcTypes();
                foreach ($types as $type => $value){
                    if ($type == 'CA' || $type == 'NE' || $type == 'CC' || $type == 'VC'){
                        $paymentTitle = Mage::getStoreConfig('payment/' . $paymentCode . '/title');
                        $methods[$paymentCode] = $paymentTitle;
                        $totalSum = 0;

                        $orderTotals = Mage::getModel('sales/order')->getCollection()
                            ->join(array('payment' => 'sales/order_payment'), 'main_table.entity_id=parent_id', 'method')
                            //->addAttributeToFilter('status', Mage_Sales_Model_Order::STATE_COMPLETE)
                            ->addAttributeToFilter('created_at', array('from' => $data['from2']))
                            ->addAttributeToFilter('is_pos', array('eq' => '1'))
                            ->addAttributeToFilter('method', array('eq' => $paymentCode))
                            //->addAttributeToFilter('method', array('eq'=>'cashondelivery'))
                            ->addAttributeToSelect('grand_total')
                            ->getColumnValues('grand_total');

                        $totalSum = array_sum($orderTotals);//var_dump($totalSum);die;
                        $totalSum = Mage::helper('core')->currency($totalSum, true, false);

                        $session = Mage::getSingleton('core/session');
                        $total = $session->getData('grand_total' . $type);
                        $start_day = floatval($data['start_day_' . $type]);
                        $end_day = floatval($data['end_day_' . $type]);
                        $result = $end_day - $start_day - $total;

                        $discrepancies = Mage::getModel('pos/discrepancies');
                        $models = $discrepancies->getCollection()
                            ->addFieldToFilter('created_at', $data['from2'])
                            ->addFieldToFilter('payment_method', $value);

                        $dismodel = $models->getFirstItem();
                        if (count($dismodel)) {
                            $dismodel = $discrepancies->load($dismodel->getData('pos_discrepancies_id'));
                            $dismodel->setData('start_day', $data['start_day_' . $type]);
                            $dismodel->setData('end_day', $data['end_day_' . $type]);
                            $dismodel->setData('sales_captured', $total);
                            $dismodel->setData('payment_method', $value);
                            //$discrepancies->setData('created_at', date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())));
                            $dismodel->setData('created_at', $data['from2']);
                            $dismodel->setData('discrepancies', $result);
                            try {
                                if($data['start_day_'.$type]!='' AND $data['end_day_'.$type])
                                    $dismodel->save();

                                $this->_redirect('*/*/create');
                                //$session->addSuccess('Updated sucessfully');
                            } catch (Exception $e) {
                                $session->addError('Error');
                            }
                        } else {
                            $discrepancies->setData('start_day', $data['start_day_' . $type]);
                            $discrepancies->setData('end_day', $data['end_day_' . $type]);
                            $discrepancies->setData('sales_captured', $total);
                            $discrepancies->setData('payment_method', $value);
                            //$discrepancies->setData('created_at', date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time())));
                            $discrepancies->setData('created_at', $data['from2']);
                            $discrepancies->setData('discrepancies', $result);

                            try {
                                if($data['start_day_'.$type]!='' AND $data['end_day_'.$type])
                                    $discrepancies->save();

                                $this->_redirect('*/*/create');
                                //$session->addSuccess('Add a pos sucessfully');
                            } catch (Exception $e) {
                                $session->addError('Error');
                            }
                        }
                    }
                }
            }
        }

    }

    public function exportCsvAction()
    {
        $fileName   = 'discrepancies.csv';
        $grid       = $this->getLayout()->createBlock('pos/adminhtml_order_discrepancies_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName   = 'discrepancies.xml';
        $grid       = $this->getLayout()->createBlock('pos/adminhtml_order_discrepancies_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

    /**
     * Delete held order
     */
    public function deleteOrderAction(){
        $quoteIds = $this->getRequest()->getPost('entity_id');
        if (!empty($quoteIds)) {
            try {
                foreach ($quoteIds as $quoteId) {
                    $quote = $this->_getSession()
                        ->setStoreId(1)
                        ->setQuoteId((int)$quoteId)
                        ->getQuote();
                    $quote->delete();
                }
            }catch(Exception $e) {
                echo $e->getMessage();
            }
        }
        $this->_redirect('*/*/create');
    }
}