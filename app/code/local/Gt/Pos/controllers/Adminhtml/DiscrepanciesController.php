<?php
class Gt_Pos_Adminhtml_DiscrepanciesController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Orders Inchoo'));
        $this->loadLayout();
        $this->_setActiveMenu('pos');
        $this->_addContent($this->getLayout()->createBlock('gt_pos/adminhtml_order_discrepancies'));
        $this->renderLayout();
    }
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('gt_pos/adminhtml_order_discrepancies_grid')->toHtml()
        );
    }


    public function exportCsvAction()
    {
        $fileName   = 'discrepancies.csv';
        $grid       = $this->getLayout()->createBlock('gt_pos/adminhtml_order_discrepancies_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName   = 'discrepancies.xml';
        $grid       = $this->getLayout()->createBlock('gt_pos/adminhtml_order_discrepancies_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}