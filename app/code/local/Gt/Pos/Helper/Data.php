<?php
/**
 * @category    Gt
 * @package     Gt_Pos
 * @author    	thuan<thuan@spreadgrace.com.sg>
 */
class Gt_Pos_Helper_Data extends Mage_Core_Helper_Data
{
    protected $_addressForm;
	
	public function getFile($filename)
    {
        return Mage::getBaseUrl('media').$filename;
    }

    public function getUserAvatar($user)
    {
        if($user->getAvatar()){
            return Mage::getBaseUrl('media') . 'pos/avatar' . $user->getAvatar();
        }else{
            return Mage::getBaseUrl('media') . 'pos/avatar/placeholder.jpg';
        }
    }
	
	protected function _getAddressForm()
    {
        if (is_null($this->_addressForm)) {
            $this->_addressForm = Mage::getModel('customer/form')
                ->setFormCode('adminhtml_customer_address')
                ->setStore($this->getStore());
        }
        return $this->_addressForm;
    }
	
	public function getEmptyAddress(){
		$emptyAddress = Mage::getModel('customer/address');
//            ->setCountryId(Mage::helper('core')->getDefaultCountry($this->getStore()));

        $emptyAddress->setData(
            array(
                'firstname' => '',
                'lastname' => '',
                'company' => '',
                'street' => Array(),
                'city' => '',
                'region' => '',
                'region_id' => '',
                'country_id' => Mage::helper('core')->getDefaultCountry($this->getStore()),
                'postcode' => '',
                'telephone' => '',
            )
        );
			
		return $emptyAddress;
	}

    /**
     * Get default address for order created by POS
     *
     * @return Mage_Customer_Model_Address
     */
    public function getDefaultAddress()
    {
		$defaultAddress = Mage::getModel('customer/address');
        $countryId = Mage::helper('core')->getDefaultCountry($this->getStore());
        $regionsCollection = Mage::getResourceModel('directory/region_collection')->addCountryFilter($countryId)
            ->getFirstItem();
        $regionId = $regionsCollection->getId();
		$defaultAddress->setData(
			array(
				'firstname' => 'Pos',
				'lastname' => 'Guest',
				'company' => 'n/a',
				'street' => Array('n/a'),
				'city' => 'n/a',				
				'region' => 'n/a',
                'region_id' => $regionId,
				'country_id' => Mage::helper('core')->getDefaultCountry($this->getStore()),
				'postcode' => '00000',
				'telephone' => '000000000',
			)
		);
		return $defaultAddress;
	}

    /**
     * Get address object of customer, return address from collection or default address
     * @param Mage_Customer_Model_Customer $customer
     * @return Mage_Customer_Model_Address
     */
    public function getCustomerAddress($customer)
    {
        if($customer->getId()){
            if(!isset($this->_customerAddress)){
                $address = $customer->getAddressesCollection()
                    ->getFirstItem();

                //Customer have addresses
                if($address->getId()){
                    $this->_customerAddress = $address;
                    return $address;
                }

                //Customer don't have address, use default address
                $this->_customerAddress = $this->getDefaultAddress();
            }

            return $this->_customerAddress;
        }
	}

    public function getCustomerAddressJson($address)
    {
        $addressForm = $this->_getAddressForm();
        $addressForm->setEntity($address);
        return json_decode($addressForm->outputData(Mage_Customer_Model_Attribute_Data::OUTPUT_FORMAT_JSON));
    }
	
	public function getStore(){
		return Mage::app()->getStore();
	}
}