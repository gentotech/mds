<?php

/** @var $installer Mage_Sales_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'sales/order'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('pos_discrepancies'))
    ->addColumn('pos_discrepancies_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'auto_increment' => true,
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Discrepancies Id')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
    ), 'Created At')
    ->addColumn('start_day', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
    ), 'Start Of Day')
    ->addColumn('end_day', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
    ), 'End Of Day')

    ->addColumn('payment_method', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
    ), 'Payment Method')
    ->addColumn('sales_captured', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
    ), 'Sales Captured')
    ->addColumn('discrepancies', Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
    ), 'Discrepancies');

$installer->getConnection()->createTable($table);

$installer -> endSetup();