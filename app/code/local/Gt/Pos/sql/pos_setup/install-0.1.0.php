<?php
/*
 * Date: 14/12/12
 * Time: 09:52
 * @author Ocean <ocean@gentotech.com>
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer -> startSetup();

/**
 * Add two columns for customer avatar and quote
 */
$installer->run("
ALTER TABLE `{$this->getTable('admin/user')}` ADD `user_quote` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `email` ,
ADD `avatar` VARCHAR( 255 ) NULL DEFAULT NULL AFTER `user_quote`
");

/**
 * Add two columns for idicator quote and order is created from POS
 */
$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'),'is_pos',array(
        'TYPE' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'length' => 5,
        'unsigned' => true,
        'DEFAULT' => '0',
        'comment' => 'Define if quote is create from POS',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'),'is_pos_saved',array(
        'TYPE' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'length' => 5,
        'unsigned' => true,
        'DEFAULT' => '0',
        'comment' => 'Define if quote is saved from POS',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/quote'),'pos_customer_id',array(
        'TYPE' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 10,
        'nullable' => true,
        'default' => null,
        'comment' => 'Define POS customer id (new, guest, number)',
    ));

$installer->getConnection()
    ->addColumn($installer->getTable('sales/order'),'is_pos',array(
        'TYPE' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'length' => 5,
        'unsigned' => true,
        'DEFAULT' => '0',
        'comment' => 'Define if order is create from POS',
    ));

$installer -> endSetup();