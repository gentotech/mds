<?php
/*
 * Date: 14/12/12
 * Time: 09:52
 * @author Ocean <ocean@gentotech.com>
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer -> startSetup();

/**
 * Add two columns for customer avatar and quote
 */

/**
 * Add two columns for idicator quote and order is created from POS
 */


$installer->getConnection()
    ->addColumn($installer->getTable('sales/order_grid'),'is_pos',array(
        'TYPE' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'length' => 5,
        'unsigned' => true,
        'DEFAULT' => '0',
        'comment' => 'Define if order is create from POS',
    ));

$installer -> endSetup();