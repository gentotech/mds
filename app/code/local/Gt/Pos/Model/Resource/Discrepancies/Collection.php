<?php
class Gt_Pos_Model_Resource_Discrepancies_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('pos/discrepancies');
    }
}