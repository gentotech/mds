<?php

class Gt_Pos_Model_Resource_Discrepancies extends Mage_Core_Model_Resource_Db_Abstract{

    protected function _construct()
    {
        $this->_init('pos/discrepancies', 'pos_discrepancies_id');
    }
}