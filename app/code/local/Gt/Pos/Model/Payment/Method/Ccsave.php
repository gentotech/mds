<?php
 /** 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @Author     Ocean <ocean@gentotech.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Gt_Pos_Model_Payment_Method_Ccsave extends Mage_Payment_Model_Method_Ccsave
{
    public function validate()
    {
        $info = $this->getInfoInstance();

        if($info instanceof Mage_Sales_Model_Order_Payment){
            $saleObject = $info->getOrder();
        }else{
            $saleObject = $info->getQuote();
        }

        if($saleObject && $saleObject->getIsPos()){
            return $this;
        }

        return parent::validate();
    }
}