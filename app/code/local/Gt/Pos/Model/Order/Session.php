<?php
class Gt_Pos_Model_Order_Session extends Mage_Adminhtml_Model_Session_Quote
{
	/**
     * Retrieve quote model object
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        if (is_null($this->_quote)) {
            $this->_quote = Mage::getModel('sales/quote')
                ->setIsPos(true);//Flag for POS quote

            if ($this->getStoreId() && $this->getQuoteId()) {
                $this->_quote->setStoreId($this->getStoreId())
                    ->load($this->getQuoteId());
            }
            elseif($this->getStoreId() && $this->hasCustomerId()){
                $this->_quote->setStoreId($this->getStoreId())
                    ->setCustomerGroupId(Mage::getStoreConfig(self::XML_PATH_DEFAULT_CREATEACCOUNT_GROUP))
                    ->assignCustomer($this->getCustomer())
                    ->setIsActive(false)
                    ->save();
                $this->setQuoteId($this->_quote->getId());
            }
            $this->_quote->setIgnoreOldQty(true);
            $this->_quote->setIsSuperMode(true);
        }
        return $this->_quote;
    }

    public function setQuote($quote)
    {
        $this->_quote = $quote;
        return $this;
    }

    public function setCustomerId($customerId)
    {
        $this->setData('customer_id',$customerId);
        //Check customer id is numeric because we use "new", "guest" for customer id
        if(!is_numeric($customerId)){
            $this->setData('customer_id',0);
        }
        $this->setData('pos_customer_id',$customerId);
    }
}