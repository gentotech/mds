<?php
$installer = $this;

$installer->startSetup();

$installer->addAttribute("quote", "shipping_store_id", array("type"=>"int"));
$installer->addAttribute("quote", "shipping_store_name", array("type"=>"varchar"));
$installer->addAttribute("order", "shipping_store_id", array("type"=>"int"));
$installer->addAttribute("order", "shipping_store_name", array("type"=>"varchar"));

$installer->endSetup(); 