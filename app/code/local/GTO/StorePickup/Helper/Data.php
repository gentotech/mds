<?php
class GTO_StorePickup_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function applyPickupStore(Varien_Event_Observer $observer)
	{
		$order = $observer->getEvent()->getOrder();
		// $order = Mage::getModel('sales/order')->loadByIncrementId($order->getRealOrderId());
		
		$cart = Mage::getModel('checkout/cart')->getQuote()->getData();
		$shipping_store_id = $cart['shipping_store_id'];
		$shipping_store_name = $cart['shipping_store_name'];

		if (isset($shipping_store_id) && !empty($shipping_store_id)){
			$order->setShippingStoreId($shipping_store_id);
			$order->setShippingStoreName($shipping_store_name);		
			$order->setStoreId($shipping_store_id);	
		}	
			
		// $order->save();
		// var_dump($order->getStoreId());
		// die;
	}
	
}
	 