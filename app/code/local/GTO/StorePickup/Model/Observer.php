<?php
class GTO_StorePickup_Model_Observer
{
	public function checkout_controller_onepage_save_shipping_method($observer)
	{		
		$request = $observer->getEvent()->getRequest();
		$quote =  $observer->getEvent()->getQuote();

		$shipping_store_id = $request->getPost('shipping_store_id', '');
		if (isset($shipping_store_id) && !empty($shipping_store_id)){
			$quote->setShippingStoreId($shipping_store_id);
			// $quote->setStoreId($shipping_store_id);	
			$quote->setShippingStoreName($request->getPost('shipping_store_name'));			
			$quote->save();
		}		
		
		return $this;
	}	
}
