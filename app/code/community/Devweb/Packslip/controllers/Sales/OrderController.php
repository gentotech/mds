<?php
require_once 'Mage/Adminhtml/controllers/Sales/OrderController.php';

class Devweb_Packslip_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{
	function getPdf($shipments)
	{
		$pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf2($shipments);
		$page = current($pdf->pages);
		
		return $pdf;
	}
	
	public function packAction()
	{
		$orderIds = $this->getRequest()->getPost('order_ids');
		$flag = false;
        $check = array();
		if (!empty($orderIds)) {
            $i = 0;
            $number = 14;
            $count = count($orderIds);
			foreach ($orderIds as $orderId) {
                $i++;
				if (!$orderId) {
					continue;
				}
				$order = Mage::getModel('sales/order');
				$order->load($orderId);
				$shipment = Mage::getModel('packslip/order_shipment');
				$shipment->setOrder($order);
				$shipment->setIncrementId('PRE-'. $order->getRealOrderId());
				$flag = true;
                $check[] = $shipment;
                $a = $i/14;
                if (is_int($a)){
                    if (!isset($pdf)) {
                        $pdf = $this->getPdf($check);
                    } else {
                        $pages = $this->getPdf($check);
                        $pdf->pages = array_merge($pdf->pages, $pages->pages);
                    }
                    unset($check);
                    $check = array();
                }else{
                    if($i==$count){
                        if (!isset($pdf)) {
                            $pdf = $this->getPdf($check);
                        } else {
                            $pages = $this->getPdf($check);
                            $pdf->pages = array_merge($pdf->pages, $pages->pages);
                        }
                    }
                }
			}


            if ($flag) {
                return $this->_prepareDownloadResponse(
                    'packingslip'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(),
                    'application/pdf'
                );
            } else {
                $this->_getSession()->addError($this->__('There are no printable documents related to selected orders.'));
                $this->_redirect('adminhtml/*/');
            }
		}
		
		$this->_redirect('adminhtml/*/');
	}
}
