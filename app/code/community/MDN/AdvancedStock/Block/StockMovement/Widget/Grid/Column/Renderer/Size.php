<?php

class MDN_AdvancedStock_Block_StockMovement_Widget_Grid_Column_Renderer_Size extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {

        $product_id = $row->getData('sm_product_id');

        $size_value_id = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product_id ,'size','');
        $size_label = Mage::getModel('catalog/product')
                                     ->setData('size', $size_value_id)
                                     ->getAttributeText('size');

        $options = $this->getColumn()->getOptions();
        $_product = Mage::getModel('catalog/product')->getCollection()->getAttribute('size')->getFrontEnd()->getSelectOptions($product_id);

        return $size_label;
    }
}
