<?php
class MDN_AdvancedStock_Block_StockMovement_Widget_Grid_Column_Renderer_Quantity extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
        if(Mage::getSingleton('core/session')->getFromDate()){
            $_fromdate = Mage::getSingleton('core/session')->getFromDate();
        }
        if(Mage::getSingleton('core/session')->getToDate()){
            $_todate = Mage::getSingleton('core/session')->getToDate();
        }
		$product_id = $row->getData('sm_product_id');
        $stock_id = $this->getColumn()->getStockid();
        //Get all the warehouses
        //var_dump($row->getData());
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $query = 'SELECT SUM(sm_qty) as sum_qty_stock FROM stock_movement WHERE sm_product_id='.$product_id.' AND sm_source_stock='.$stock_id;
        if(isset($_fromdate))
            $query .=' AND sm_date >"'.$_fromdate.'"';
        if(isset($_todate))
            $query .=' AND sm_date <"'.$_todate.'"';
        $arrWarehouse = $readConnection->fetchAll($query);
        return ($arrWarehouse[0]['sum_qty_stock']!=null)?$arrWarehouse[0]['sum_qty_stock']:'0';
	}
}
?>