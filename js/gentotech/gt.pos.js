//@author: DaiDuong
(function($){
    $(document).ready(function (e) {
        Pos.init();
    });
	
	Pos = {}

    Pos.init = function(){		
		Pos.Core.initBarcodeListenner();
        Pos.Utils.Lightbox.init();       
	}

    Pos.Core = {
        registerMainTabs: function(){
            $('#action_tabs').tabs({
                activeType: 1,
                activate: function(event,ui){
                    $(ui.newTab)
                        .find('a')
                        .addClass('active');
                    $(ui.oldTab)
                        .find('a')
                        .removeClass('active');
                }
            });
        },
        initOrderedGrid: function(){
            new varienGrid('itemsOrderedGrid');
        },
		initBarcodeListenner: function(){
			$('body').BarcodeListener(function(code){
				Pos.Order.addProductByBarcode(code);
			});
		}
    }

    Pos.Order = {
        loadBlockUrl: false,       
        skinUrl: false,
		printUrl: false,
        submitUrl: false,
        getDataUrl: false,
        loadingAreas: false,
        productBlocks: ['items_ordered','total','product_info','promotions_list','message','shipping_method','payment_method','actions'],
        customerBlocks: ['customer_info','customer_address','shipping_method','total','message'],
        paymentShippingBlocks: ['total','payment_method','shipping_method','actions','message'],       
        customerId: false,
		paymentMethod: false,
		shippingMethod: false,
        storeId: false,
        currencyId: false,
        shippingAsBilling: true,		
		init: function(opts){
			this.loadBlockUrl  	= opts.loadBlockUrl;
            this.getDataUrl     = opts.getDataUrl;
			this.skinUrl       	= opts.skinUrl;
			this.submitUrl     	= opts.submitUrl;
			this.printUrl 		= opts.printUrl;
			
            this.customerId     = opts.customer_id ? opts.customer_id : false;
            this.storeId        = opts.store_id ? opts.store_id : false;
            this.currencyId     = opts.currencyId ? opts.currency_id : false;
			
			this.paymentMethod  = opts.payment_method ? opts.payment_method : false;
			this.shippingMethod = opts.shipping_method ? opts.shipping_method : false;
		},
		getLoadBlockUrl: function(block){
			if(block.constructor == Array){
				this.loadingAreas = $.unique(block);
				return this.loadBlockUrl + 'block/' + this.loadingAreas;
			}
			
			this.loadingAreas = [];
			return this.loadBlockUrl;
            
        },              
        prepareParams : function(params){
            if (!params) {
                params = {};
            }
			
            if (!params.customer_id) {
				if(this.customerId === false){
					params.customer_id = 'guest';
				}else{
					params.customer_id = this.customerId;
				}                
            }
            if (!params.store_id) {
                params.store_id = this.storeId;
            }
            if (!params.currency_id) {
                params.currency_id = this.currencyId;
            }
            if (!params.form_key) {
                params.form_key = $('input[name="form_key"]').val();
            }           
            if(!this.shippingAsBilling){
                this.shippingAsBilling = true;
                params.shipping_as_billing = 1;
            }
			if(!this.paymentMethod){
				params['payment[method]'] = 'checkmo';
			}else{
				params['payment[method]'] = this.paymentMethod;				
			}
			if(this.shippingMethod){
				params['order[shipping_method]'] = this.shippingMethod;		
			}

            return params;
        },
		serializeData: function(container){
			var data = {};
			if($(container).length == 1){
				$(container + ' :input').serializeArray().each(function(item){
					data[item.name] = item.value;
				});
			}
			return data;
		},
        loadArea: function(areas, indicator, params){
			if (indicator === true) indicator = 'html-body';
			
			params = this.prepareParams(params);
			
			if(!this.validateAddress() && $('input:radio[name="customer[mode]"]:checked').val() != 'new' && !params.disable_address_check){
				if($.inArray('customer_address',areas) < 0){
					areas = $.merge(areas,['customer_address']);
				}
				
				if(!params.update_address){
					params['update_address'] = 1;
				}
			}
			
			var url = this.getLoadBlockUrl(areas);
			
			params.json = true;
			if (!this.loadingAreas) this.loadingAreas = [];
			if (indicator) {
				new Ajax.Request(url,{
					parameters: params,
					loaderArea: indicator,
					onSuccess: function(transport)
					{
						var response = transport.responseText.evalJSON();

						if(response){
							this.loadAreaResponse(response);

                            //update data when have restore_order
                            if(params.restore_order && parseInt(params.restore_order) > 0){
                                this.getOrderData();
                            }
						}else{
							alert(transport.responseText);
						}
					}.bind(Pos.Order)
				});
			}else{
				new Ajax.Request(url, {parameters:params,loaderArea: indicator});
			}          
        },
        loadAreaResponse: function(response){
            if (response.error) {
                alert(response.message);
            }

            if(response.ajaxExpired && response.ajaxRedirect) {
                setLocation(response.ajaxRedirect);
            }
			
            if(this.loadingAreas.indexOf('message'==-1)) this.loadingAreas.push('message');
            for(var i=0; i<this.loadingAreas.length; i++){
                var id = this.loadingAreas[i];
                if(this.getAreaById(id)){
                    if (response[id]) {
						this.getAreaById(id).html(response[id] ? response[id] : '');
						
						var messageEl = this.getAreaById('message');
						if(response['message'] && messageEl){
							if(messageEl.data('clear_time_out') == "undefined"){
								clearTimeout(messageEl.data('clear_time_out'));
								messageEl.data('clear_time_out',"undefined")
							}
							var timeout = setTimeout(function(){
								messageEl.html('');
							},10000);
							
							messageEl.data('clear_time_out',timeout);
						}
                    }
                }
				if(response['order_info'] && response['order_info'] != ''){							
					Pos.Utils.Lightbox.setSuccess(response['order_info']);
				}
            }
        },
        setCustomerMode: function(mode){
            var loadArea = false;
            switch(mode)
            {
                case 'new':
                    $('#div_cust_new').show();
                    $('#div_cust_existing').hide();
                    $('#div_selected_customer').show();
                    $('#newsletter').attr('disabled',false);
					
					if(this.customerId != 'new'){
                        this.customerId = 'new';
                        loadArea = true;
                    }
					
                    break;
                case 'existing':
                    $('#div_cust_new').hide();
                    $('#div_cust_existing').show();
                    $('#div_selected_customer').show();
                    $('#newsletter').attr('disabled',true);
                    break;
                case 'guest':
                    $('#div_cust_new').hide();
                    $('#div_cust_existing').hide();
                    $('#div_selected_customer').hide();
                    $('#newsletter').attr('disabled',true);

                    if(parseInt(this.customerId) != parseInt(this.guestId)){
                        // this.customerId = this.guestId;
                        this.customerId = 'guest';
                        loadArea = true;
                    }
                    break;
            }

            if(loadArea){
                this.loadArea(this.customerBlocks,true,{reset_shipping:1, update_address:1});
            }
        },
		saveData: function(data){
			this.loadArea(false,false,data);
		},
        selectCustomer: function(customerId){
              if($('#alreadySelectCustomer').val() == customerId){
                  return;
              }
              this.customerId = customerId;

              this.loadArea(this.customerBlocks,true,{reset_shipping:1,update_address:1});
        },
		bindAddressFields: function(){
			var self = this;
			var input = $('#pos-customer_address :input').each(function(){
				$(this).bind('change',function(event){
					self.changeAddressField(event);
				}) 
			});
		},
		changeAddressField: function(event){
			var element = event.target;

			var re = /[^\[]*\[([^\]]*)_address\]\[([^\]]*)\](\[(\d)\])?/;
			var matchRes = element.name.match(re);

			if (!matchRes) {
				return;
			}

			var type = matchRes[1];
			var name = matchRes[2];
			var data;
			
			data = this.serializeData('#pos-customer_address');
			data.reset_shipping = 1;
			// this.shippingAsBilling = false;
			this.loadArea(['total','payment_method','shipping_method','actions'],false,data);
		},
		validateAddressField: ['firstname','lastname','street0','city','country_id','postcode','telephone'],
		validateAddress: function(){
			for(var i = 0; i < this.validateAddressField.length; i++){				
				var element = $('#order-billing_address_' + this.validateAddressField[i]);				
				if(element.length > 0){					
					if(element.val().trim() == ''){
						return false;
					}
				}
			}
			return true;
		},
		switchPaymentMethod: function(method,addition){			
			this.setPaymentMethod(method,addition); 
			
			var data = {};
			data['payment[cc_type]'] = addition;
            this.loadArea(this.paymentShippingBlocks,true,data);
		},
		setPaymentMethod : function(method/*,addition*/){
			if (this.paymentMethod && $('#payment_form_'+this.paymentMethod).length > 0) {
				var form = $('#payment_form_' + this.paymentMethod);
				
				if (form.length > 0){
					form.hide();
					form.find(':input').each(function(i,field) {
						field.disabled = true;
					});
				}				
			}
			
			var self = this;
			
			if ($('#payment_form_' + method).length > 0){
				this.paymentMethod = method;				
				// if(method == 'ccsave' && typeof(addition) != 'undefined'){
					// $('#ccsave_cc_type').val(addition);
				// }
				var form = $('#payment_form_' + method);
				
				if (form.length > 0) {
				   form.show();
				   form.find(':input').each(function(i,field) {
					   field.disabled = false;
					   if (!field.bindChange) {
							field.bindChange = true;						  
							field.method = method;
							$(field).bind('change', function(event){
								self.changePaymentData(event);
							})
						}
					});
				}				
			}
		},
		changePaymentData : function(event){
			var elem = event.target;
			if(elem && elem.method){
				var data = this.getPaymentData(elem.method);
				if (data) {
					 this.loadArea(['card_validation'], true, data);
				} else {
					return;
				}
			}
		},
		getPaymentData : function(currentMethod){
			if (typeof(currentMethod) == 'undefined') {
				if (this.paymentMethod) {
					currentMethod = this.paymentMethod;
				} else {
					return false;
				}
			}
			var data = {};
			var fields = $('#payment_form_' + currentMethod).find(':input');
			for(var i=0;i<fields.length;i++){
				data[fields[i].name] = fields[i].getValue();
			}
			if ((typeof data['payment[cc_type]']) != 'undefined' && (!data['payment[cc_type]'] || !data['payment[cc_number]'])) {
				return false;
			}
			return data;
		},
        toggleInvoice: function(el){
            this.saveData({is_create_invoice: el.checked});
        },
		selectShipping: function(method){
			this.shippingMethod = method;
            this.loadArea(this.paymentShippingBlocks,true);
		},
		loadShippingRates : function(){			
			this.loadArea(['shipping_method', 'totals'], true, {collect_shipping_rates: 1});
		},
		setComment: function(){
			var data = this.serializeData('#pos-customer_comment');			
			this.saveData(data);
		},
		addProduct: function(pid){			
            this.loadArea(this.productBlocks,true,{add_product: pid});
		},
		addProductByBarcode: function(barcode){
			barcode = parseInt(barcode);
			
			if(barcode == "NaN"){
				alert('Invalid barcode!');
				return;
			}
			
			this.loadArea(this.productBlocks,true,{barcode: barcode});
		},
        removeQuoteItem: function(quoteId){            
            this.loadArea(this.productBlocks,true,{remove_item: quoteId});
        },
		checkQtyAndUpdate: function(event,skipCheckKeycode){
			var el = event.target;
			var qty = parseInt(el.value);
			
			// if(event.keyCode  != '13' && !skipCheckKeycode){				
				// return;
			// }

			if(qty != "NaN" && $(el).data('quote-id') != "undefined"){
				if(qty > 0){
					this.updateQty($(el).data('quote-id'),parseInt(el.value));
				}else{
					this.removeQuoteItem($(el).data('quote-id'));
				}					
			}			
		},
        increaseQty: function(quoteId){
            var $input = $('#qty_' + quoteId);

            this.updateQty(quoteId,parseInt($input.val()) + 1);
        },
        decreaseQty: function(quoteId){
            var $input = $('#qty_' + quoteId);

            if(parseInt($input.val()) < 2){
                this.removeQuoteItem(quoteId);
                return;
            }

            this.updateQty(quoteId,parseInt($input.val()) - 1);
        },
        updateQty: function(quoteId,qty){
            var params = {
                update_items: 1					
            }
            params['item[' + quoteId + '][use_discount]'] = 1;
            params['item[' + quoteId + '][qty]'] = qty;

            this.loadArea(this.productBlocks,true,params);
        },
        getAreaById: function(id){
			var el = $('#pos-' + id);
			if(el.length == 1){
				return el;
			}else{
				return false;
			}          
        },
        getOrderData: function(){
            new Ajax.Request(this.getDataUrl,{
                loaderArea: false,
                onSuccess: function(transport)
                {
                    var response = transport.responseText.evalJSON();

                    if(response){
                        this.customerId     = response.customer_id ? response.customer_id : false;
                        this.paymentMethod  = response.payment_method ? response.payment_method : false;
                        this.shippingMethod = response.shipping_method ? response.shipping_method : false;
                    }else{
                        alert(transport.responseText);
                    }
                }.bind(Pos.Order)
            });
        },
		placeOrder: function(){
			if($('.pos-order_item_row').length < 1){
                alert('Can\'t submit empty order.');
                return;
            }
			var data = this.serializeData('#pos-data');
			data.place_order = 1;
			this.loadArea(['order_info'],true,data);
		},
        createPos: function(){
            if($('.pos-order_item_row').length < 1){
                alert('Can\'t submit empty order.');
                return;
            }
            var data = this.serializeData('#pos-data');
            data.place_order = 1;
            this.loadArea(['order_info'],true,data);
        },
        saveOrder: function(){
            this.loadArea(['data'],true,{save_order:1});
            this.shippingAsBilling = true;
            this.shippingMethod = false;
            this.paymentMethod = false;
            this.customerId = false;
        },
        restoreOrder: function(quote_id){
			this.loadArea(['data'],true,{restore_order: quote_id,disable_address_check:1});
        },
        refresh: function(){
            Pos.Utils.Lightbox.hide();

            this.shippingAsBilling = true;
			this.shippingMethod = false;
			this.paymentMethod = false;
            this.customerId = false;        

            this.loadArea(['data'],true);
        },
		printReceipt: function(orderId){
			if(!orderId){
				alert('Order id is null.');
				return;
			}
			
			var printFrame = $('<iframe/>',{				
				style:'display:none;',
				src: Pos.Order.printUrl + 'order_id/' + orderId
			});
			
			var dataBlock = this.getAreaById('data');
			dataBlock.append(printFrame);
		}
	}
	
	Pos.Utils = {}

    Pos.Utils.Calculator = {
        currencySymbol: null,
        orderTotal: 0,
        givenAmount: 0,
        changeAmount: 0,
        elementOrderTotal: '#cc_order_total',
        elementGivenAmount: '#cc_given_amount',
        elementChangeAmount: '#cc_change',
        init: function(data){
            this.currencySymbol = data.currencySymbol != '' ? data.currencySymbol : '$';
        },
        formatPrice: function(price){
            return String(this.currencySymbol) + String(parseFloat(price).toFixed(2));
        },
        //TODO: round price
        doRound: function(price){
            var price = parseFloat(price).toFixed(2);

        },
        show: function(){
            var calculator = this;
            Dialog.alert(
                $('#pos-change_calculator').html(),
                {
                    id: "productInfosDialog",
                    buttonClass:"dialogButton",
                    width:800,
                    height:500,
                    zIndex:200,
                    okLabel: 'Close',
                    destroyOnClose:true,
                    draggable:true,
                    recenterAuto: true,
                    className: "alphacube",
                    onShow: function(){
                        calculator.setPrice(calculator.elementOrderTotal,calculator.getOrderTotal());
                        calculator.refresh();
                    },
                    ok: function(win){
                        calculator.refreshOrderTotal();
                        return true;
                    }
                }
            );
        },
        setPrice: function(el,price){
            if(el == '#cc_given_amount'){
                $(el).val(parseFloat(price).toFixed(2));
            }else{
                $(el).html(this.formatPrice(price));
            }
        },
        getOrderTotal: function(){
            return $('#order_total_value').val();
        },
		collectTotals:function(){
			var changeAmount = parseFloat(this.givenAmount) - parseFloat(this.getOrderTotal());
            this.changeAmount = changeAmount > 0 ? changeAmount : 0;
		},
        addAmount: function(value){
            this.givenAmount += parseFloat(value);
            this.collectTotals();
            this.refresh();
        },
		processCustomAmount: function(element){
			var value = $(element).val();
			this.givenAmount = parseFloat(value);
			this.collectTotals();
            this.refresh();
		},
        refresh: function(){
            this.setPrice(this.elementGivenAmount,this.givenAmount);
            this.setPrice(this.elementChangeAmount,this.changeAmount);
        },
        refreshOrderTotal: function(){
            $('#order_amount_received').html(this.formatPrice(this.givenAmount));
            $('#order_change').html(this.formatPrice(this.changeAmount));
        },
        resetAmount: function(){
            this.givenAmount = 0;
            this.changeAmount = 0;
            this.refresh();
        }
    }

    Pos.Utils.Calculator2 = {
        currencySymbol: null,
        orderTotal: 0,
        givenAmount: 0,
        changeAmount: 0,
        elementOrderTotal: '#cc_order_total',
        elementGivenAmount: '#cc_given_amount',
        elementChangeAmount: '#cc_change',
        init: function(data){
            this.currencySymbol = data.currencySymbol != '' ? data.currencySymbol : '$';
        },
        formatPrice: function(price){
            return String(this.currencySymbol) + String(parseFloat(price).toFixed(2));
        },
        //TODO: round price
        doRound: function(price){
            var price = parseFloat(price).toFixed(2);

        },
        show: function(){
            var calculator = this;
            Dialog.alert(
                $('#pos-change_calculator2').html(),
                {
                    id: "productInfosDialog",
                    buttonClass:"dialogButton",
                    width:800,
                    height:500,
                    zIndex:200,
                    okLabel: 'Close',
                    destroyOnClose:true,
                    draggable:true,
                    recenterAuto: true,
                    className: "alphacube",
                    onShow: function(){
                        calculator.setPrice(calculator.elementOrderTotal,calculator.getOrderTotal());
                        calculator.refresh();
                    },
                    ok: function(win){
                        calculator.refreshOrderTotal();
                        return true;
                    }
                }
            );
        },
        setPrice: function(el,price){
            if(el == '#cc_given_amount'){
                $(el).val(parseFloat(price).toFixed(2));
            }else{
                $(el).html(this.formatPrice(price));
            }
        },
        getOrderTotal: function(){
            return $('#order_total_value').val();
        },
        collectTotals:function(){
            var changeAmount = parseFloat(this.givenAmount) - parseFloat(this.getOrderTotal());
            this.changeAmount = changeAmount > 0 ? changeAmount : 0;
        },
        addAmount: function(value){
            this.givenAmount += parseFloat(value);
            this.collectTotals();
            this.refresh();
        },
        processCustomAmount: function(element){
            var value = $(element).val();
            this.givenAmount = parseFloat(value);
            this.collectTotals();
            this.refresh();
        },
        refresh: function(){
            this.setPrice(this.elementGivenAmount,this.givenAmount);
            this.setPrice(this.elementChangeAmount,this.changeAmount);
        },
        refreshOrderTotal: function(){
            $('#order_amount_received').html(this.formatPrice(this.givenAmount));
            $('#order_change').html(this.formatPrice(this.changeAmount));
        },
        resetAmount: function(){
            this.givenAmount = 0;
            this.changeAmount = 0;
            this.refresh();
        }
    }

	Pos.Utils.Lightbox = {
        model: false,
        init: function(){
            var self = this;

            $('#pos-lightbox').qtip({
                prerender: true,
                content: {
                    title: {
                        button: true,
                        text: 'Order Success'
                    },
                    text: this.getDefaultLoadingHtml()
                },
                events:{
                    render: function(event,api){
                        self.model = api;
                    }
                },
                position: {
                    my: 'center',
                    at: 'center',
                    target: $(window)
                },
                show: {
                    solo: true,
                    modal: true
                },
                hide: false,
                style: 'qtip-light qtip-rounded lightbox'
            })
        },
        getDefaultLoadingHtml: function(){
            return '<div style="width: 200px;height: 110px;margin-top: 75px;text-align: center;"><img src="' + Pos.Order.skinUrl + 'gt_pos/images/ajax-loader.gif" alt="Loading..."></div>';
        },
        getModel: function(){
            return this.model;
        },
        reset: function(){
            this.set('content.title.text','Loading...');
            this.set('content.text', this.getDefaultLoadingHtml());
        },
        set: function(target,data){
            this.model.set(target,data);
        },
		setTitle: function(title){
			this.set('content.title.text',title);
		},
        setSuccess: function(data){console.debug('setsuccess');
            this.setTitle('Success');
            this.set('content.text',data);
			this.show();
        },		
		setError: function(data){
            this.setTitle('Error');

            var html = '<div>' +
                            '<h2>Error...</h2>' +
                            '<p>' + data +'</p>' +
                            '<button onclick="Pos.Utils.Lightbox.hide()">Continue</button>';

            this.set('content.text',html);
			
			this.show();
        },
        show: function(){
            this.model.show();
        },
        hide: function(){
            this.model.hide();
        }
    }
	
	/**	
	 * BarcodeListener v 1.1 (jQuery plug-in)
	 * http://code.google.com/p/jquery-barcodelistener/
	 * made by Gregorio Pellegrino
	 * edited by Ocean <ocean@gentotech.com> 
	 */
	jQuery.fn.BarcodeListener = function(callback) {		
		$("body").prepend("<form id=\"29LLRUZk\" style=\"opacity:0;display:block;width:0;height:0;\"><input type=\"text\" name=\"L3ZitQdL\" id=\"L3ZitQdL\" /></form>");
		$(document).keypress(function(e) {
			//check if in input or textarea
			if (document.activeElement)
			{
				if ((document.activeElement.type == 'text') || (document.activeElement.tagName.toLowerCase() == 'textarea'))
				{
					return true;
				}
			}
			
			if(e.which == 13){
				if($("#L3ZitQdL").val() != ''){
					$("#29LLRUZk").submit();
				}				
				return;
			}			
			
			$("#L3ZitQdL").val($("#L3ZitQdL").val()+String.fromCharCode(e.which));	
				
			// event propagation
			e.cancelBubble = true;
			e.returnValue = false;

			if (e.stopPropagation) {
				e.stopPropagation();
				e.preventDefault();
			}
		});						
		
		$("#29LLRUZk").submit(function() {
			code = $("#L3ZitQdL").val();
			
			if (typeof(callback) == "function") {
				callback(code);
			}
			
			$("#L3ZitQdL").val('');
			return false;
		});
	}
})(jQuery)