(function($){
    Designer = function(configs){
        this.initialize(configs);
    }

    $.extend(Designer,{
        TAB_STORE_ITEMS: 0,
        TAB_CUSTOMER_ITEMS: 1
    });

    $.extend(Designer.prototype,{
        pageSize: 16,
        $form: false,
        $canvas: false,
        $instruction: false,
        productDetailUrl: false,
        productListUrl: false,
        customerUploadListUrl: false,
        validator: false,
        currentTab: 0,
        pagerOptions: false,
        isCallSubmit: false,
        isInitPaging: false,
        isInitRequest: false,
        galleryItemSize: 85,
        keepAspectRatio: true,
        maxResizeRatio: 2.5,
        minResizeRatio: 3,
        initialize: function(configs){
            this.$form = configs.form ? $(configs.form) : false;
            this.$canvas = this.$form.find('.canvas');
            this.$instruction = this.$form.find('#builder-instructions');

            var designer = this;
            this.pagerOptions = {
                callback: function(page_index,jq){
                    designer.pageSelectCallback(page_index,jq,designer);
                },
                link_to: 'javascript:void(0);',
                items_per_page: designer.pageSize,
                num_edge_entries: 0,
                num_display_entries: 4,
                prev_text : '&laquo; Prev',
                next_text : 'Next &raquo;'
            }

            this.productDetailUrl = configs.productDetailUrl ? configs.productDetailUrl : false;
            this.productListUrl = configs.productListUrl ? configs.productListUrl : false;
            this.customerUploadListUrl = configs.customerUploadListUrl ? configs.customerUploadListUrl : false;
            this.skinUrl = configs.skinUrl ? configs.skinUrl : false;

            this.registerTabs();
            this.registerItemClick();
            this.registerDocumentClick();
            this.registerLoadWaiting();
            this.initDroppable();
        },
        registerTabs: function(){
            var designer = this;
            $('#control-tabs').tabs({
                select: function(event,ui){
                    designer.tabSelectCallback(event, ui, designer);
                }
            });
        },
        registerLoadWaiting: function(){
            $('<div id="busy"><img src="' + this.skinUrl + 'images/designer/ajax-loader.gif" alt="loading"></div>')
            .hide()
            .ajaxStart(function () {
                $(this).show();
            })
            .ajaxStop(function () {
                $(this).hide();
            })
            .prependTo('#control-tabs');
        },
        registerItemClick: function(){
            var designer = this;
            $('.ui-wrapper').live('mousedown', function(){
                designer.selectItem($(this));
            });
        },
        registerDocumentClick: function(){
            var designer = this;
            $(document).keyup(function(e) {
                var _k = e.which;
                if (_k == 46) designer.deleteSelectedItem();
                if (_k == 32) designer.cloneSelectedItem();
                if (_k == 27) designer.unSelectAll();
                if (_k == 13) designer.bringForward();
            }).click(function (e) {
                var $t = $(e.target);
                if (!($t.is('.ui-wrapper') || $t.parents().is('.ui-wrapper'))){
                    designer.unSelectAll();
                }
            });
        },
        tabSelectCallback: function(event, ui, designer){
            designer.currentTab = ui.index;
            if(ui.index == Designer.TAB_CUSTOMER_ITEMS){
                designer.loadCustomerItems();
            }else{
                if (!designer.getCurrentCategory() && !designer.getSearchValue()) {
                    designer.showCategoryList();
                }
                else {
                    designer.isInitPaging = true;
                    designer.loadItems();
                }
            }
        },
        pageSelectCallback: function(page_index,jq,designer) {
            var pageNum = page_index + 1;
            if (page_index >= 0 && !designer.isInitRequest) {
                var data = {
                    page_num: pageNum
                }

                designer.loadItems(data);
            }
            if(designer.isInitRequest){
                designer.isInitRequest = false;
            }
        },
        initPaging: function(itemCount) {
            $(".pagination-bar").show();
            $("#numProducts").empty();

            this.isInitRequest = true;
            $("#pagination").pagination(itemCount, this.pagerOptions);

            $("#numProducts").append(itemCount + " products found");
            this.isInitPaging = false;
        },
        initDroppable: function(){
            if (typeof $('body').droppable != 'undefined') {
                var designer = this;
                designer.$canvas.droppable({
                    activeClass: 'ui-state-hover',
                    accept: '.builder-item.ui-draggable',
                    tolerance: 'intersect',
                    drop: function(e, ui){
                        var $draggedItem = $('.ui-draggable-dragging');
                        var left = designer.getItemOffset($draggedItem).left
                        var top = designer.getItemOffset($draggedItem).top;
                        var $thumb = $draggedItem.find('img');
                        var imgUrl = $thumb.data('builder-image');
                        var productId = $thumb.data('product-id');

                        var itemWidth = $thumb.width() * 2;
                        var itemHeight = $thumb.height() * 2;

                        var newImageOptions = {
                            src: imgUrl
                        }

                        if(productId){
                            newImageOptions['data-product-id'] = productId;
                        }

                        var $newImg = $('<img>',newImageOptions)
                            .css({
                                width: itemWidth,
                                minHeight: 10,
                                height: itemHeight,
                                padding: 5,
                                left: left,
                                top: top,
                                cursor: 'move'
                            });

                        $newImg
                            .ready(function () {
                                designer.makeInteractive($newImg, $thumb, true)
                                    .append('<input type="hidden" name="items[]" value=""/>');
                            });

                        designer.updateItemOption();
                        designer.refreshInstruction();
                    }
                })
            }
        },
        getItemOffset: function ($item) {
            var _pos = $item.offset();
            var _pos2 = this.$canvas.offset();
            var _left = _pos.left - _pos2.left;
            var _top = _pos.top - _pos2.top;
            _left = _left > 350 ? 350 : _left;
            _left = _left < 0 ? 0 : _left;
            _top = _top > 228 ? 228 : _top;
            _top = _top < 0 ? 0 : _top;
            return {left: _left, top: _top};
        },
        makeInteractive: function ($builderImage, $itemThumb, isSelected) {
            var designer = this;

            var thumbSize = {}
            if($itemThumb instanceof jQuery){
                thumbSize.w = $itemThumb.width() * 2;
                thumbSize.h = $itemThumb.height() * 2;
            }else if(typeof $itemThumb == 'object'){
                thumbSize.w = $itemThumb.w;
                thumbSize.h = $itemThumb.h;
            }

            var optResize = {
                minWidth: thumbSize.w / designer.minResizeRatio,
                minHeight: thumbSize.h / designer.minResizeRatio,
                maxWidth: thumbSize.w * designer.minResizeRatio,
                maxHeight: thumbSize.h * designer.maxResizeRatio,
                aspectRatio: designer.keepAspectRatio,
                handles: 'n, e, s, w, ne, se, sw, nw',
                stop: function(){
                    designer.updateItemOption();
                }
            };

            var optDrag = {
                containment: designer.$canvas,
                stop: function(){
                    designer.updateItemOption();
                }
            };

            $builderImage.appendTo(designer.$canvas)
                .resizable(optResize)
                .parent()
                .draggable(optDrag);

            if(isSelected){
                designer.selectItem($builderImage.parent());
            }

            return $builderImage.parent();
        },        
        refreshInstruction: function(){
            if (this.getCanvasItemsCount() > 0) {
                this.$instruction.hide();
            } else {
                this.$instruction.show();
            }
        },
        getCanvasItems: function(){
            return this.$canvas.find('.ui-wrapper');
        },
        getCanvasItemsCount: function(){
            return this.getCanvasItems().size();
        },
        selectItem: function ($item) {
            this.unSelectAll();
            $item.addClass('ui-selected')
                .find('.ui-resizable-handle', this.$canvas)
                .show();
        },
        unSelectAll: function () {
            this.$canvas.find('.ui-selected').removeClass('ui-selected');
            $('.ui-resizable-handle', this.$canvas).hide();
        },
        getSelectedItem: function(){
            return this.$canvas.find('.ui-selected');
        },
        updateItemOption: function(){
            var $selectedItem = this.getSelectedItem();
            var $itemImage = $selectedItem.find('img');
            var $itemHiddenField = $selectedItem.find('input[type=hidden]');

            var attr = {};
            attr.product_id = $itemImage.data('product-id');
            attr.img_src = $itemImage.attr('src');
            attr.width = parseFloat($selectedItem.css('width')) - 5;
            attr.height = parseFloat($selectedItem.css('height')) - 5;
            attr.top = parseFloat($selectedItem.css('top'));
            attr.left = parseFloat($selectedItem.css('left'));

            $itemHiddenField.val(this.jsonToString(attr));
        },
        cloneSelectedItem: function(){
            var $selectedItem = this.$canvas.find('.ui-selected img.ui-resizable');
            var $clone = $selectedItem.clone();

            if ($clone.length){
                $clone.appendTo(this.$canvas);
            }
            else{
                return false;
            }

            var designer = this;
            var selectedItemPosition = this.getSelectedItem().position();
            var thumb = {
                w: $selectedItem.resizable('option','minWidth') * designer.minResizeRatio,
                h: $selectedItem.resizable('option','minHeight') * designer.minResizeRatio
            }
            var $newItem = this.makeInteractive($clone,thumb)
                .append('<input type="hidden" name="items[]" value=""/>');

            $newItem
                .css({
                    position: 'absolute',
                    left: selectedItemPosition.left,
                    top: selectedItemPosition.top,
                    opacity: 0
                })
                .animate({
                        opacity: 1,
                        left: selectedItemPosition.left + 20,
                        top: selectedItemPosition.top + 20
                    }, 300
                );
        },
        bringForward: function(){
            this.getSelectedItem()
                .insertAfter(this.$canvas.find('.ui-wrapper:last:not(.ui-selected)'));
        },
        bringBackward: function () {
            this.getSelectedItem()
                .insertBefore(this.$canvas.find('.ui-wrapper:first:not(.ui-selected)'));
        },
        deleteSelectedItem: function () {
            var $selectedItem = this.getSelectedItem();
            var designer = this;
            $selectedItem.fadeOut(200, function () {
                $(this).find('img.ui-resizable')
                    .remove()
                    .end()
                    .remove();
                designer.refreshInstruction();
            });
        },
        deleteAllItems: function(){
            var designer = this;
            designer.getCanvasItems().fadeOut(200, function(){
                $(this).find('img.ui-resizable')
                    .remove()
                    .end()
                    .remove();
                designer.refreshInstruction();
            });
        },
        selectCategory: function(categoryId, categoryName) {
            this.setCurrentCategory(categoryId);
            $('#catalog-nav')
                .find('.cell_right')
                .before(
                '<td class="cell cat-nav-item" style="width: 99%;">' +
                    '<span class="refinementcontrol" style="display: inline; visibility: inherit;">'+
                        '<span class="left">'+
                            categoryName +
                        '</span>'+
                        '<span class="close left clickable" onclick="designer.showCategoryList();">'+
                            'x'+
                        '</span>'+
                    '</span>'+
                '</td>'
            );
            $('#catalog-nav').fadeIn();

            this.isInitPaging = true;
            this.loadItems();
        },
        getCurrentCategory: function(){
            return this.currentCategory;
        },
        setCurrentCategory: function (categoryId){
            this.currentCategory = categoryId;
        },
        getCurrentTab: function(){
            return this.currentTab;
        },
        loadCustomerItems: function() {
            this.isInitPaging = true;
            this.loadItems(0,1);
        },
        showCategoryList: function(){
            this.showCategoryElement()
            this.setCurrentCategory('');

            $('#catalog-nav')
                .hide()
                .find('.cat-nav-item')
                .remove();

            $('.pagination-bar').hide();

            this.flushGalleryElement();
        },
        showCategoryElement: function(){
            $('.categories').fadeIn();
        },
        hideCategoryElement: function(){
            $('.categories').fadeOut();
        },
        flushGalleryElement: function(){
            $('#gallery')
                .html('')
                .fadeOut();
        },
        flushCustomerGalleryElement: function(){
            $('#customerGallery')
                .html('')
                .fadeOut();
        },
        searchBoxKeypressCallback: function(e, designer){
            designer.unSelectAll();
            code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                if(!e) var e = window.event;

                e.cancelBubble = true;
                e.returnValue = false;

                if (e.stopPropagation) {
                    e.stopPropagation();
                    e.preventDefault();
                }

                designer.runSearch();
            }
        },
        getSearchValue: function(){
            var searchValue = $("#txtSearch").val();
            if (searchValue == "Enter Search Term" || searchValue == ''){
                return false;
            }

            searchValue = searchValue.replace("/'/g", "&apos;");
            return searchValue;
        },
        //TODO: fix catalog nav
        runSearch: function(e) {
            if(e){
                e.preventDefault();
            }

            if(!this.getSearchValue()){
                return;
            }

            this.isInitPaging = true;
            this.isInitRequest = true;

            var data = {
                search_value: this.getSearchValue()
            }

            this.loadItems(data);
        },
        prepareLoadItemsParams: function(params){
            if(!params){
                params = {};
            }

            if(!params.page_size){
                params.page_size = this.pageSize;
            }

            if(!params.page_num){
                params.page_num = 1;
            }

            if(!params.category_id){
                params.category_id = this.getCurrentCategory();
            }

            return params;
        },
        getLoadItemsUrl: function(){
            if(this.getCurrentTab() == Designer.TAB_STORE_ITEMS){
                return this.productListUrl;
            }else{
                return this.customerUploadListUrl;
            }
        },
        loadItems: function(params){
            this.hideCategoryElement();

            this.flushGalleryElement();
            this.flushCustomerGalleryElement();

            params = this.prepareLoadItemsParams(params);

            var designer = this;
            $.ajax({
                type: "POST",
                url: this.getLoadItemsUrl(),
                data: params /*{'pageSize': pageSize, 'pageNumber': pageNum, 'catId': categoryId, 'searchString' : searchString}*/,
                dataType: "json",
                success: function (data) {
                    if(data.redirect){
                        location.href = data.redirect;
                        return;
                    }

                    if(data.error){
                        alert(data.message);
                        return;
                    }

                    var list = data.d.items;
                    if(list.length > 0){
                        $.each(list, function () {
                            designer.addItemToGallery(this);
                        });
                    }else{
                        designer.addEmptyResultToGallery();
                    }

                    if (designer.isInitPaging) {
                        designer.initPaging(data.d.totalResults);
                    }

                    designer.getSelectedGalleryElement().fadeIn();
                    $('.pagination-bar').show();
                },
                error: function (response) {
                    alert('An error occur!');
                    log(response);
                    return;
                }
            });
        },
        getSelectedGalleryElement: function(){
            if(this.getCurrentTab() == Designer.TAB_STORE_ITEMS){
                return $('#gallery');
            }else{
                return $('#customerGallery');
            }
        },
        addEmptyResultToGallery: function() {
            var $list = $("<div class='no-items-found'>Sorry, no products found for this search term</div>")
                .appendTo(this.getSelectedGalleryElement());
            return $list;
        },
        addItemToGallery: function(item) {
//            var $listItem = $("<div class='builder-item'><img data-product-id='" + item.product_id + "' title='" + item.name + "' data-builder-image='"+ item.builder_image +"' src='" + item.thumb_image + "'></div>")
//                .appendTo(this.getSelectedGalleryElement());

            var $div = $('<div/>',{class:'builder-item'});

            var imageOptions = {
                src: item.thumb_image,
                'data-builder-image': item.builder_image
            }

            if(item.product_id){
                imageOptions['data-product-id'] = item.product_id;
            }
            if(item.name){
                imageOptions['title'] = item.name;
            }

            var $image = $('<img>',imageOptions)
                .appendTo($div);

            $div.appendTo(this.getSelectedGalleryElement());

            this.positionGalleryItem($div,this.galleryItemSize);

            $div.draggable({
                revert: 'invalid',
                helper: 'clone',
                cursor: 'move'
            });

            var designer = this;
            if(item.product_id){
                $div.click(function(){
                    designer.getProductDetail(item.product_id);
                });
            }
        },
        positionGalleryItem: function($item,itemSize){
            $item.ready(function(){
                var $newImg = $item.find('img');
                $newImg.load(function(){
                    var imgMarginTop = (Math.floor((itemSize - $newImg.height()) / 2));
                    $newImg.css('margin-top', imgMarginTop + 'px');
                });
            });
        },
        getProductDetail: function(productId){
            var designer = this;
            $.ajax({
                type: "POST",
                url: designer.productDetailUrl,
                data: {product_id : productId},
                dataType: "json",
                success: function (data) {
                    if(data.redirect){
                        location.href = data.redirect;
                        return;
                    }

                    if(data.error){
                        alert(data.message);
                        return;
                    }

                    $.fancybox(data.product_info,{

                    });
                },
                error: function (response) {
                    alert('There is error in the request.');
                }
            });
        },
        save: function(){
            if(this.verifyCanvas()){
                this.showForm();
            }
        },
        showForm: function(){
            var designer = this;
            $.fancybox({
                href: '#save-media-form-container',
                maxWidth: 400,
                minHeight: 500,
                afterShow: function(){
                    designer.validator = new Validation('save-media-form-container');
                },
                afterClose: function(){
                    if(designer.isCallSubmit){
                        designer.$form.submit();
                    }
                }
            });
        },
        submit: function(){
            if(this.validator && this.validator.validate()){
                this.isCallSubmit = true;
                $.fancybox.close();
            }
        },
        verifyCanvas: function(){
            if(this.getCanvasItemsCount() == 0){
                alert('Can not save empty design!');
                return false;
            }

            return true;
        },
        jsonToString: function(object){
            var t = typeof (object);
            if (t != "object" || object === null) {
                // simple data type
                if (t == "string") object = '"'+object+'"';
                return String(object);
            }
            else {
                // recurse array or objectect
                var n, v, json = [], arr = (object && object.constructor == Array);
                for (n in object) {
                    v = object[n]; t = typeof(v);
                    if (t == "string") v = '"'+v+'"';
                    else if (t == "object" && v !== null) v = JSON.stringify(v);
                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
                return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
            }
        }
    })
})(jQuery)

var debug = null;
function log(msg, trace) {
    try {
        console.debug(msg);
        if(trace) {
            console.trace();
        }
    } catch(e) {}
    if((debug!=null)&&(debug.style.display != 'none')) {
        debug.value = msg + "\n" + debug.value;
    }
}

//
//$j = jQuery.noConflict();
//
//var totalPages = 0;
//var globalCPage = 0;
//var pageSize = 12;
//var pageNum = 1;
//var isPagingInit = true;
//var pagingOpts;
//var minPageSize = 9;
//var pageSizeDisplay;
//var isInitRequest = true;
//var isOClicked = true;
//var options, a;
//var bDraft = false;
//var bSave = true;
//var myUploadClicked = false;
//var currentTab = 0;
//
//$j(document).ready(function($){
//    //get data from html
//    imageUrl = $('#imageUrl').val();
//    strLocalHost = $('#baseUrl').val();
//
//    CID = $('#customerId').val();
//
//    var $gallery = $('#gallery');
//    var $trash = $('#droppable');
//    var $canvas = $('div.canvas');
//
//    var $btnDelete = $('#btnDelete');
//    var $btnClone = $('#btnClone');
//    var $btnForward = $('#btnForward');
//    var $btnBackward = $('#btnBackward');
//
//    var $btnClear = $('#btnClear');
//
////    if (typeof $('body').droppable != 'undefined') {
////        $canvas.droppable({
////            activeClass: 'ui-state-hover',
////            accept: '#gallery > div, #customerGallery > div',
////            tolerance: 'intersect',
////            drop: function (e, ui) {
////                var $dragged = $('.ui-draggable-dragging');
////                var _left = itemOffset($dragged).left
////                var _top = itemOffset($dragged).top;
////                var $thumb = $dragged.find('img');
////                var _imgUrl = $thumb.attr('srclarge');
////                var itemid = $thumb.data('product-id');
////
////                var itemWidth = $thumb.width() * 2;
////                var itemHeight = $thumb.height() * 2;
////
////                if(itemid != null)
////                    imageId = 'data-product-id="' + itemid + '"';
////                else
////                    imageId = '';
////                var $newImg = $('<img src="' + _imgUrl + '" ' + imageId + '/>').css({ width: itemWidth, minHeight: 10, height: itemHeight, padding: 5, left: _left, top: _top, cursor: 'move' });
////
////                $newImg.ready(function () {
////                    makeInteractive($newImg, itemWidth, itemHeight, true).append('<input type="hidden" name="items[]" value=""/>');
////                });
////                updateAttributeForSave();
////                enableInstruction();
////            },
////            over: function () {
////
////            },
////            out: function () {
////
////            }
////        });
////
////        log('inited droppable');
////    }
//
//    // offset relative to canvas
//    var itemOffset_ = function ($item) {
//        var _pos = $item.offset();
//        var _pos2 = $canvas.offset();
//        var _left = _pos.left - _pos2.left;
//        var _top = _pos.top - _pos2.top;
//        _left = _left > 350 ? 350 : _left;
//        _left = _left < 0 ? 0 : _left;
//        _top = _top > 228 ? 228 : _top;
//        _top = _top < 0 ? 0 : _top;
//        return { left: _left, top: _top };
//    };
//
//    var makeInteractive_ = function ($img, W, H, bSelect) {
//        var _optResize = {
//            minWidth: W / 3,
//            minHeight: H / 3,
//            maxWidth: W * 2.7,
//            maxHeight: H * 2.7,
//            aspectRatio: true,
//            handles: 'n, e, s, w, ne, se, sw, nw',
//            stop: updateAttributeForSave
//        };
//        var _optDrag = {
//            containment: $canvas,
//            stop: updateAttributeForSave
//        };
//        $img.appendTo($canvas).resizable(_optResize).parent().draggable(_optDrag);
//
//        if (bSelect)
//            setSelected($img.parent());
//        return $img.parent();
//    };
//
//    var updateAttributeForSave_ = function(event, ui){
//
//        var itemInput = $('.ui-selected').find('input');
//        var itemImg = $('.ui-selected').find('img');
//
//        var attr = {};
//        attr.product_id = itemImg.data('product-id');
//        attr.imgSrc = itemImg.attr('src');
//        attr.width = parseFloat(itemImg.parent().css('width')) - 5;
//        attr.height = parseFloat(itemImg.parent().css('height')) - 5;
//        attr.top = parseFloat(itemImg.parent().css('top'));
//        attr.left = parseFloat(itemImg.parent().css('left'));
//        itemInput.val(JSON.stringify(attr));
//    };
//
//    // actions
//    var setSelected__ = function ($sel) {
//        unSelectAll();
//        $sel.addClass('ui-selected').find('.ui-resizable-handle', $canvas).css({ display: 'block' });
//    };
//
//    var unSelectAll_ = function () {
//        $canvas.find('.ui-selected').removeClass('ui-selected');
//        $('.ui-resizable-handle', $canvas).css({ display: 'none' });
//    };
//
//    var deleteSelected_ = function () {
//        $canvas.find('.ui-selected').fadeOut(200, function () {
//            $(this).find('img.ui-resizable').remove().end().remove();
//            enableInstruction();
//        });
//    };
//
//    var deleteAll_ = function () {
//        $canvas.find('.ui-wrapper').fadeOut(200, function () {
//            $(this).find('img.ui-resizable').remove().end().remove();
//            enableInstruction();
//        });
//    };
//
//    var itemsCount_ = function () {
//        return $canvas.find('.ui-wrapper').size();
//    };
//
//    var enableInstruction_ = function () {
//        var $builderInstructions = $("#builder-instructions");
//        if (itemsCount() > 0) {
//            $builderInstructions.css({ display: 'none' });
//        } else {
//            $builderInstructions.css({ display: 'block' });
//        }
//        //builder - instructions
//    };
//
//    var cloneSelected_ = function () {
//        var $clone = $canvas.find('.ui-selected img.ui-resizable').clone();
//        if ($clone.length)
//            $clone.appendTo($canvas);
//        else
//            return false;
//        var _pos = $canvas.find('.ui-selected').position();
//        var $newItem = makeInteractive($clone).append('<input type="hidden" name="items[]" value=""/>');
//        $newItem.css({ position: 'absolute', left: _pos.left, top: _pos.top, opacity: 0 })
//            .animate({ opacity: 1, left: _pos.left + 20, top: _pos.top + 20 }, 300);
//
//    };
//
//    var bringForward_ = function () {
//        $canvas.find('.ui-selected').insertAfter($canvas.find('.ui-wrapper:last:not(.ui-selected)'));
//    };
//
//    var bringBackward_ = function () {
//        $canvas.find('.ui-selected').insertBefore($canvas.find('.ui-wrapper:first:not(.ui-selected)'));
//    };
//
////    $btnDelete.click(function () { deleteSelected(); $(this).blur(); return false; });
////    $btnClear.click(function () { deleteAll(); $(this).blur(); return false; });
////    $btnClone.click(function () { cloneSelected(); $(this).blur(); return false; });
////    $btnForward.click(function () { bringForward(); $(this).blur(); return false; });
////    $btnBackward.click(function () { bringBackward(); $(this).blur(); return false; });
//
////    $('.ui-wrapper').live('mousedown', function () {
////        setSelected($(this));
////    });
////
////    $(document).keyup(function (e) {
////        var _k = e.which;
////        if (_k == 46) deleteSelected();
////        if (_k == 32) cloneSelected();
////        if (_k == 27) unSelectAll();
////        if (_k == 13) bringForward();
////    }).click(function (e) {
////            var $t = $(e.target);
////            if (!($t.is('.ui-wrapper') || $t.parents().is('.ui-wrapper')))
////                unSelectAll();
////        });
////
////    $(function () {
////        var testTextBox = $('#txtSearch');
////        var code = null;
////        testTextBox.keypress(function (e) {
////            unSelectAll();
////            code = (e.keyCode ? e.keyCode : e.which);
////            if (code == 13) {
////                if(!e) var e = window.event;
////
////                e.cancelBubble = true;
////                e.returnValue = false;
////
////                if (e.stopPropagation) {
////                    e.stopPropagation();
////                    e.preventDefault();
////                }
////
////                if($(this).val() != ''){
////                    runSearch();
////                }
////            }
////        });
////    });
////
////    $('<div id="busy"><img src="' + imageUrl + 'designer/ajax-loader.gif" alt="loading"></div>')
////        .hide()
////        .ajaxStart(function () {
////            $(this).show();
////        })
////        .ajaxStop(function () {
////            $(this).hide();
////        })
////        .prependTo('#all-items');
////
////    log('inited paging options');
////    pagingOpts = getOptionsFromForm();
//
//});
//
//function tabSelectFunction_(event, ui){
//    if(ui.index == 1){
//        myUploadClicked = true;
//        loadCustomerItem();
//    }else{
//        //load all item
//        myUploadClicked = false;
//        if (($j('#selCategory').val() == '' || $j('#selCategory').val() == null) && $j('#txtSearch').val() == 'Enter Search Term') {
//            showCategory();
//        }
//        else {
//            isPagingInit = true;
//            getItems(getCurrentCatId(), pageNum);
//        }
//    }
//    currentTab = ui.index;
//}
//
//function loadCustomerItem_() {
//    if (CID != '') {
//        $j('#pnMyItems').addClass('hidden');
//        PageSize = 16;
//        isPagingInit = true;
//        getItems(0, 1, CID);
//    }
//}
//
//
//function getOptionsFromForm_() {
//    return {
//        callback: pageSelectCallback,
//        items_per_page: pageSize,
//        num_edge_entries: 0,
//        num_display_entries: 4,
//        prev_text : '&laquo; Prev',
//        next_text : 'Next &raquo;'
//    }
//}
//
////callback fire when click on page number
//function pageSelectCallback_(page_index, jq) {
//    pageNum = page_index + 1;
//    if (page_index >= 0 && isInitRequest == false && isOClicked == false) {
//        if (myUploadClicked)
//            getItems(0, 1, CID);
//        else
//            getItems(getCurrentCatId(), pageNum);
//    }
//    isInitRequest = false;
//    isOClicked = false;
//}
//
//function initPaging_(numRecords) {
//    log('init paging');
//    $j(".pagination-bar").css({ display: 'block' });
//    $j("#numProducts").empty();
//    if (isPagingInit)
//        $j("#pagination").pagination(numRecords, pagingOpts);
//    $j("#numProducts").append(numRecords + " products found");
//    isPagingInit = false;
//}
//
//function verifyCanvas_(){
//    if($j('#cnv').find('.ui-wrapper').length == 0){
//        alert('Can not save empty design!');
//        return;
//    }
//};
//
//
//function setCatOption_(catId, catName) {
//    setCurrentCatId(catId);
//    $j('#catalog-nav').find('.cell_right').before(
//        '<td class="cell cat-nav-item" style="width: 99%;">' +
//            '<span class="refinementcontrol" style="display: inline; visibility: inherit;">'+
//            '<span class="left">'+
//            catName +
//            '</span>'+
//            '<span class="close left clickable" onclick="showCategory();">'+
//            'x'+
//            '</span>'+
//            '</span>'+
//            '</td>'
//    );
//    $j('#catalog-nav').fadeIn();
//    // isFavourites = false;
//    runSearch();
//}
//
//function showCategory_(){
//    //show category list
//    $j('.categories').fadeIn();
//    setCurrentCatId('');
//    $j('#catalog-nav').hide().find('.cat-nav-item').remove();
//    $j('.pagination-bar').hide();
//    //flush current gallery items
//    $j('#gallery').html('').fadeOut();
//}
//
//function getCurrentCatId_(){
//    return $j('#selCategory').val();
//}
//
//function setCurrentCatId_(id){
//    log('current category id: ' + id);
//    return $j('#selCategory').val(id);
//}
//
//function getCurrentSearchString(){
//    var searchString = $j("#txtSearch").val();
//    if (searchString == "Enter Search Term")
//        return '';
//    else{
//        searchString = searchString.replace("/'/g", "&apos;");
//        return searchString;
//    }
//}
//
////TODO: fix catalog nav
//function runSearch() {
//    isPagingInit = true;
//    isInitRequest = true;
//    getItems(getCurrentCatId());
//}
//
//
//function getItems(categoryId, currentPageNumber, customerId) {
//    currentPageNumber = currentPageNumber || 1;
//    var searchString = getCurrentSearchString();
//    globalCPage = currentPageNumber;
//
//    log('hide category list');
//    $j('.categories').hide();
//
//    //flush current gallery items
//    log('flush current gallery items');
//    $j('#gallery').html('');
//    $j('#customerGallery').html('');
//
//    var src = "";
//    if (myUploadClicked) {
//        src = strLocalHost + "designer/media/customerlist/";
//    }
//    else {
//        src = strLocalHost + "designer/media/productlist/";
//    }
//
//    $j.ajax({
//        type: "POST",
//        url: src,
//        data: {'pageSize': pageSize, 'pageNumber': pageNum, 'catId': categoryId, 'searchString' : searchString, 'customerId' : customerId },
//        dataType: "json",
//        success: function (data) {
//            if(data.redirect){
//                location.href = data.redirect;
//                return;
//            }
//            var locationToAdd = '';
//            if(currentTab == 0){
//                locationToAdd = 'gallery';
//            }else{
//                locationToAdd = 'customerGallery';
//            }
//            var list = data.d.items;
//            if(list.length > 0){
//                $j.each(list, function () {
//                    addRow(this, locationToAdd);
//                });
//            }else
//                addRowEmpty(locationToAdd);
//
//            if (isPagingInit) {
//                initPaging(data.d.totalResults);
//                //reset page number to 1 when init request
//                pageNum = 1;
//            }
//            $j('#gallery').fadeIn();
//            $j('.pagination-bar').show();
//        },
//        error: function (response) {
//            alert(response.responseText);
//        }
//    });
//}
//
//function positionImages_(img) {
//    var imgPadding = (Math.floor((85 - $j(img).height()) / 2));
//    $j(img).css('margin-top', imgPadding + 'px');
//}
//
//function addRowEmpty_(where) {
//    var $list = $j("<div class='no-items-found'>Sorry, no products found for this search term</div>").appendTo($j("#" + where));
//}
//
//function addRow_(item, where) {
//    if(item.itemId != null)
//        var $list = $j("<div class='builder-item'><img id='" + item.itemId + "' data-product-id='" + item.itemId + "' title='" + item.name + "' data-builder-image='"+ item.bigImage +"' src='" + item.thumbImage + "'></div>").appendTo($j("#" + where));
//    else
//        var $list = $j("<div class='builder-item'><img data-builder-image='"+ item.bigImage +"' src='" + item.thumbImage + "'></div>").appendTo($j("#" + where));
//    $list.ready(
//        function () {
//            var $newImg = $list.find('img');
//            $newImg.load(function (){
//                positionImages(this);
//            });
//        }
//    );
//
//    $list.draggable({
//        revert: 'invalid',
//        helper: 'clone',
//        cursor: 'move'
//    });
//
//    if(item.itemId != null){
//        $list.click(function () {
//            showDetails(item.itemId);
//        });
//    }
//
//    log('added item: ' + item.name);
//}
//
//function showDetails_(productId){
//    $j.ajax({
//        type: "POST",
//        url: strLocalHost + "designer/media/productdetail/",
//        data: {product_id : productId},
//        dataType: "json",
//        success: function (data) {
//            if(data.redirect){
//                location.href = data.redirect;
//                return;
//            }
//
//            if(data.error){
//                alert(data.message);
//                return;
//            }
//
//            $j.fancybox(data.product_info,{
//
//            });
//        },
//        error: function (response) {
//            alert('There is error in the request.');
//        }
//    });
//}
//
//
//// implement JSON.stringify serialization
//JSON.stringify = JSON.stringify || function (object) {
//    var t = typeof (object);
//    if (t != "objectect" || object === null) {
//        // simple data type
//        if (t == "string") object = '"'+object+'"';
//        return String(object);
//    }
//    else {
//        // recurse array or objectect
//        var n, v, json = [], arr = (object && object.constructor == Array);
//        for (n in object) {
//            v = object[n]; t = typeof(v);
//            if (t == "string") v = '"'+v+'"';
//            else if (t == "objectect" && v !== null) v = JSON.stringify(v);
//            json.push((arr ? "" : '"' + n + '":') + String(v));
//        }
//        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
//    }
//};
