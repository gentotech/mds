
Validation.add('validate-nric','Please enter a valid NRIC. For example X1234567X.',function(field_value) {

    var regex = /^[A-Z]\d\d\d\d\d\d\d[A-Z]$/;
    if(field_value.match(regex)){
        return true;
    }
    return false;
});
